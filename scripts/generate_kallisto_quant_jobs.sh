# Load module to get trimmomatic jar location.
export RAP_ID=def-masirard

OUTDIR=output/kallisto
mkdir -p output/jobs
for libr1 in output/trim/*/*pair1.fastq.gz
do
  libr2=`echo $libr1 | sed -e 's/pair1/pair2/'`
  
  libname=`basename $libr1 .trim.pair1.fastq.gz`
  mkdir -p $OUTDIR/$libname

  job_name=kallisto-quant.$libname
  job_script=output/jobs/$job_name.sh

  cat << EOF > $job_script  
#!/bin/bash  
/home/efournie/kallisto_linux-v0.44.0/kallisto quant -t 6 -i output/kallisto/Homo_sapiens.GRCh38.cdna.all.fa.idx -o output/kallisto/$libname $libr1 $libr2
EOF

  sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D `pwd` -o $job_script.stdout -e $job_script.stderr -J $job_name --time=24:00:0 --mem=24G -N 1 -n 6 $job_script
done