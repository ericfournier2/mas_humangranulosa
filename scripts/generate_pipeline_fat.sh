OUTDIR=$SCRATCH/output/chip-pipeline
mkdir -p $OUTDIR

~/genpipes/pipelines/rnaseq/rnaseq.py -s '1-23' \
    -l debug \
    -r raw/readset_fat.txt \
    -d raw/design_fat.txt \
    -o $OUTDIR \
    --config ~/genpipes/pipelines/rnaseq/rnaseq.base.ini \
        ~/genpipes/pipelines/rnaseq/rnaseq.cedar.ini \
        $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini \
        input/bugfix.ini
