# Load module to get trimmomatic jar location.
module load mugqic/trimmomatic/0.36
export RAP_ID=def-masirard

OUTDIR=output/trim
mkdir -p output/trim
mkdir -p output/jobs
for libfile in raw/*R1.fastq.gz
do
  R1=$libfile
  R2=`echo $libfile | sed -e 's/R1/R2/'`
  libname=`echo $libfile | sed -e 's/raw\/.*Index_.*\.\(.*\)_R1.fastq.gz/\1/'`
  job_name=trim.$libname
  job_script=output/jobs/$job_name.sh
  
  cat << EOF > $job_script
#!/bin/bash
module load java/1.8.0_121 mugqic/trimmomatic/0.36
mkdir -p $OUTDIR/$libname
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  $R1 \
  $R2 \
  $OUTDIR/$libname/$libname.trim.pair1.fastq.gz \
  $OUTDIR/$libname/$libname.trim.single1.fastq.gz \
  $OUTDIR/$libname/$libname.pair2.fastq.gz \
  $OUTDIR/$libname/$libname.single2.fastq.gz \
  ILLUMINACLIP:input/adapters.fa:2:30:15:8:true \
  TRAILING:30 \
  MINLEN:32  
EOF

  sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D `pwd` -o $job_script.stdout -e $job_script.stderr -J $job_name --time=24:00:0 --mem=24G -N 1 -n 6 $job_script


done
