#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# RnaSeq PBSScheduler Job Submission Bash script
# Version: 3.0.1-beta
# Created on: 2018-02-10T17:14:42
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 6 jobs
#   merge_trimmomatic_stats: 1 job
#   star: 14 jobs
#   picard_merge_sam_files: 0 job... skipping
#   picard_sort_sam: 6 jobs
#   picard_mark_duplicates: 6 jobs
#   picard_rna_metrics: 6 jobs
#   estimate_ribosomal_rna: 6 jobs
#   bam_hard_clip: 6 jobs
#   rnaseqc: 2 jobs
#   wiggle: 36 jobs
#   raw_counts: 6 jobs
#   raw_counts_metrics: 4 jobs
#   cufflinks: 6 jobs
#   cuffmerge: 1 job
#   cuffquant: 6 jobs
#   cuffdiff: 3 jobs
#   cuffnorm: 1 job
#   fpkm_correlation_matrix: 2 jobs
#   gq_seq_utils_exploratory_analysis_rnaseq: 3 jobs
#   differential_expression: 1 job
#   differential_expression_goseq: 4 jobs
#   TOTAL: 126 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/scratch/efournie/output/chip-pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/RnaSeq_job_list_$TIMESTAMP
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.HI.4519.002.Index_1.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4519.002.Index_1.CTRL
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4519.002.Index_1.CTRL.0f1c4e6eb18a7dfd422d99e57a7fd023.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4519.002.Index_1.CTRL.0f1c4e6eb18a7dfd422d99e57a7fd023.mugqic.done'
#!/bin/bash \
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/CTRL && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_1.CTRL_R1.fastq.gz \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_1.CTRL_R2.fastq.gz \
  trim/CTRL/HI.4519.002.Index_1.CTRL.trim.pair1.fastq.gz \
  trim/CTRL/HI.4519.002.Index_1.CTRL.trim.single1.fastq.gz \
  trim/CTRL/HI.4519.002.Index_1.CTRL.trim.pair2.fastq.gz \
  trim/CTRL/HI.4519.002.Index_1.CTRL.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15:8:true \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/CTRL/HI.4519.002.Index_1.CTRL.trim.log
trimmomatic.HI.4519.002.Index_1.CTRL.0f1c4e6eb18a7dfd422d99e57a7fd023.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_2_JOB_ID: trimmomatic.HI.4519.002.Index_2.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4519.002.Index_2.L-FAT
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4519.002.Index_2.L-FAT.2206e1284b7de38e489c2c735552aa55.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4519.002.Index_2.L-FAT.2206e1284b7de38e489c2c735552aa55.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/L-FAT && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_2.L-FAT_R1.fastq.gz \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_2.L-FAT_R2.fastq.gz \
  trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.pair1.fastq.gz \
  trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.single1.fastq.gz \
  trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.pair2.fastq.gz \
  trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15:8:true \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.log
trimmomatic.HI.4519.002.Index_2.L-FAT.2206e1284b7de38e489c2c735552aa55.mugqic.done
)
trimmomatic_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_3_JOB_ID: trimmomatic.HI.4519.002.Index_3.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4519.002.Index_3.H-FAT
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4519.002.Index_3.H-FAT.ab4ebd28fb48b528a65a6e57b7ec5d6a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4519.002.Index_3.H-FAT.ab4ebd28fb48b528a65a6e57b7ec5d6a.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/H-FAT && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_3.H-FAT_R1.fastq.gz \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_3.H-FAT_R2.fastq.gz \
  trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.pair1.fastq.gz \
  trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.single1.fastq.gz \
  trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.pair2.fastq.gz \
  trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15:8:true \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.log
trimmomatic.HI.4519.002.Index_3.H-FAT.ab4ebd28fb48b528a65a6e57b7ec5d6a.mugqic.done
)
trimmomatic_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_4_JOB_ID: trimmomatic.HI.4519.002.Index_4.CTRL-Ins
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4519.002.Index_4.CTRL-Ins
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4519.002.Index_4.CTRL-Ins.a6b3b81ee5a3e2a2f6804ba99d073722.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4519.002.Index_4.CTRL-Ins.a6b3b81ee5a3e2a2f6804ba99d073722.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/CTRL.Ins && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_4.CTRL-Ins_R1.fastq.gz \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_4.CTRL-Ins_R2.fastq.gz \
  trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.pair1.fastq.gz \
  trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.single1.fastq.gz \
  trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.pair2.fastq.gz \
  trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15:8:true \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.log
trimmomatic.HI.4519.002.Index_4.CTRL-Ins.a6b3b81ee5a3e2a2f6804ba99d073722.mugqic.done
)
trimmomatic_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_5_JOB_ID: trimmomatic.HI.4519.002.Index_5.L-FAT-Ins
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4519.002.Index_5.L-FAT-Ins
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4519.002.Index_5.L-FAT-Ins.c4aca644035681e05a07dcae61e2e208.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4519.002.Index_5.L-FAT-Ins.c4aca644035681e05a07dcae61e2e208.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/L-FAT.Ins && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_5.L-FAT-Ins_R1.fastq.gz \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_5.L-FAT-Ins_R2.fastq.gz \
  trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.pair1.fastq.gz \
  trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.single1.fastq.gz \
  trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.pair2.fastq.gz \
  trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15:8:true \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.log
trimmomatic.HI.4519.002.Index_5.L-FAT-Ins.c4aca644035681e05a07dcae61e2e208.mugqic.done
)
trimmomatic_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_6_JOB_ID: trimmomatic.HI.4519.002.Index_6.H-FAT-Ins
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4519.002.Index_6.H-FAT-Ins
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4519.002.Index_6.H-FAT-Ins.9d383b43027e21fe5682361478331126.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4519.002.Index_6.H-FAT-Ins.9d383b43027e21fe5682361478331126.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/H-FAT.Ins && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 6 \
  -phred33 \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_6.H-FAT-Ins_R1.fastq.gz \
  /project/6009125/efournie/HumanGranulosa/raw/HI.4519.002.Index_6.H-FAT-Ins_R2.fastq.gz \
  trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.pair1.fastq.gz \
  trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.single1.fastq.gz \
  trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.pair2.fastq.gz \
  trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.single2.fastq.gz \
  ILLUMINACLIP:/cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.0/bfx/adapters-truseq.fa:2:30:15:8:true \
  TRAILING:30 \
  MINLEN:32 \
  2> trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.log
trimmomatic.HI.4519.002.Index_6.H-FAT-Ins.9d383b43027e21fe5682361478331126.mugqic.done
)
trimmomatic_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$trimmomatic_2_JOB_ID:$trimmomatic_3_JOB_ID:$trimmomatic_4_JOB_ID:$trimmomatic_5_JOB_ID:$trimmomatic_6_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.57c95128bf16995ddaab9bc2bbfcfa54.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.57c95128bf16995ddaab9bc2bbfcfa54.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Paired Reads #	Surviving Paired Reads #	Surviving Paired Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/CTRL/HI.4519.002.Index_1.CTRL.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/CTRL	HI.4519.002.Index_1.CTRL	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/L-FAT	HI.4519.002.Index_2.L-FAT	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/H-FAT	HI.4519.002.Index_3.H-FAT	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/CTRL.Ins	HI.4519.002.Index_4.CTRL-Ins	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/L-FAT.Ins	HI.4519.002.Index_5.L-FAT-Ins	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/H-FAT.Ins	HI.4519.002.Index_6.H-FAT-Ins	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /home/efournie/genpipes/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /home/efournie/genpipes/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=32 \
  --variable read_type=Paired \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.57c95128bf16995ddaab9bc2bbfcfa54.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"merge_trimmomatic_stats\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"merge_trimmomatic_stats\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: star
#-------------------------------------------------------------------------------
STEP=star
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: star_1_JOB_ID: star_align.1.HI.4519.002.Index_1.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.HI.4519.002.Index_1.CTRL
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/star/star_align.1.HI.4519.002.Index_1.CTRL.95ed277ba57b0b63989481e8b74e0541.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.HI.4519.002.Index_1.CTRL.95ed277ba57b0b63989481e8b74e0541.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment_1stPass/CTRL/HI.4519.002.Index_1.CTRL && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/star_index/Ensembl87.sjdbOverhang99 \
  --readFilesIn \
    trim/CTRL/HI.4519.002.Index_1.CTRL.trim.pair1.fastq.gz \
    trim/CTRL/HI.4519.002.Index_1.CTRL.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/CTRL/HI.4519.002.Index_1.CTRL/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_1.CTRL" 	PL:"ILLUMINA" 			SM:"CTRL" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitIObufferSize 4000000000
star_align.1.HI.4519.002.Index_1.CTRL.95ed277ba57b0b63989481e8b74e0541.mugqic.done
)
star_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_2_JOB_ID: star_align.1.HI.4519.002.Index_2.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.HI.4519.002.Index_2.L-FAT
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID
JOB_DONE=job_output/star/star_align.1.HI.4519.002.Index_2.L-FAT.ee8a4ef77815ca9da697b0bb4320fd3f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.HI.4519.002.Index_2.L-FAT.ee8a4ef77815ca9da697b0bb4320fd3f.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment_1stPass/L-FAT/HI.4519.002.Index_2.L-FAT && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/star_index/Ensembl87.sjdbOverhang99 \
  --readFilesIn \
    trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.pair1.fastq.gz \
    trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/L-FAT/HI.4519.002.Index_2.L-FAT/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_2.L-FAT" 	PL:"ILLUMINA" 			SM:"L-FAT" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitIObufferSize 4000000000
star_align.1.HI.4519.002.Index_2.L-FAT.ee8a4ef77815ca9da697b0bb4320fd3f.mugqic.done
)
star_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_3_JOB_ID: star_align.1.HI.4519.002.Index_3.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.HI.4519.002.Index_3.H-FAT
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID
JOB_DONE=job_output/star/star_align.1.HI.4519.002.Index_3.H-FAT.bd995288d30a964e4aed21780d28bf0a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.HI.4519.002.Index_3.H-FAT.bd995288d30a964e4aed21780d28bf0a.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment_1stPass/H-FAT/HI.4519.002.Index_3.H-FAT && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/star_index/Ensembl87.sjdbOverhang99 \
  --readFilesIn \
    trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.pair1.fastq.gz \
    trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/H-FAT/HI.4519.002.Index_3.H-FAT/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_3.H-FAT" 	PL:"ILLUMINA" 			SM:"H-FAT" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitIObufferSize 4000000000
star_align.1.HI.4519.002.Index_3.H-FAT.bd995288d30a964e4aed21780d28bf0a.mugqic.done
)
star_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_4_JOB_ID: star_align.1.HI.4519.002.Index_4.CTRL-Ins
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.HI.4519.002.Index_4.CTRL-Ins
JOB_DEPENDENCIES=$trimmomatic_4_JOB_ID
JOB_DONE=job_output/star/star_align.1.HI.4519.002.Index_4.CTRL-Ins.0941080e7d5435eb45ca20300e0de338.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.HI.4519.002.Index_4.CTRL-Ins.0941080e7d5435eb45ca20300e0de338.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment_1stPass/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/star_index/Ensembl87.sjdbOverhang99 \
  --readFilesIn \
    trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.pair1.fastq.gz \
    trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_4.CTRL-Ins" 	PL:"ILLUMINA" 			SM:"CTRL.Ins" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitIObufferSize 4000000000
star_align.1.HI.4519.002.Index_4.CTRL-Ins.0941080e7d5435eb45ca20300e0de338.mugqic.done
)
star_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_5_JOB_ID: star_align.1.HI.4519.002.Index_5.L-FAT-Ins
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.HI.4519.002.Index_5.L-FAT-Ins
JOB_DEPENDENCIES=$trimmomatic_5_JOB_ID
JOB_DONE=job_output/star/star_align.1.HI.4519.002.Index_5.L-FAT-Ins.e8cf99f7b7b98c6a76689245bdacb63f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.HI.4519.002.Index_5.L-FAT-Ins.e8cf99f7b7b98c6a76689245bdacb63f.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment_1stPass/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/star_index/Ensembl87.sjdbOverhang99 \
  --readFilesIn \
    trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.pair1.fastq.gz \
    trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_5.L-FAT-Ins" 	PL:"ILLUMINA" 			SM:"L-FAT.Ins" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitIObufferSize 4000000000
star_align.1.HI.4519.002.Index_5.L-FAT-Ins.e8cf99f7b7b98c6a76689245bdacb63f.mugqic.done
)
star_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_6_JOB_ID: star_align.1.HI.4519.002.Index_6.H-FAT-Ins
#-------------------------------------------------------------------------------
JOB_NAME=star_align.1.HI.4519.002.Index_6.H-FAT-Ins
JOB_DEPENDENCIES=$trimmomatic_6_JOB_ID
JOB_DONE=job_output/star/star_align.1.HI.4519.002.Index_6.H-FAT-Ins.c5a025cec8c71817ace1e16b599d9043.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.1.HI.4519.002.Index_6.H-FAT-Ins.c5a025cec8c71817ace1e16b599d9043.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment_1stPass/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins && \
STAR --runMode alignReads \
  --genomeDir /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/star_index/Ensembl87.sjdbOverhang99 \
  --readFilesIn \
    trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.pair1.fastq.gz \
    trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix alignment_1stPass/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_6.H-FAT-Ins" 	PL:"ILLUMINA" 			SM:"H-FAT.Ins" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitIObufferSize 4000000000
star_align.1.HI.4519.002.Index_6.H-FAT-Ins.c5a025cec8c71817ace1e16b599d9043.mugqic.done
)
star_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_7_JOB_ID: star_index.AllSamples
#-------------------------------------------------------------------------------
JOB_NAME=star_index.AllSamples
JOB_DEPENDENCIES=$star_1_JOB_ID:$star_2_JOB_ID:$star_3_JOB_ID:$star_4_JOB_ID:$star_5_JOB_ID:$star_6_JOB_ID
JOB_DONE=job_output/star/star_index.AllSamples.2675d30f15f8530d87b4c9778528e61d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_index.AllSamples.2675d30f15f8530d87b4c9778528e61d.mugqic.done'
module load mugqic/star/2.5.3a && \
cat \
  alignment_1stPass/CTRL/HI.4519.002.Index_1.CTRL/SJ.out.tab \
  alignment_1stPass/L-FAT/HI.4519.002.Index_2.L-FAT/SJ.out.tab \
  alignment_1stPass/H-FAT/HI.4519.002.Index_3.H-FAT/SJ.out.tab \
  alignment_1stPass/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins/SJ.out.tab \
  alignment_1stPass/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins/SJ.out.tab \
  alignment_1stPass/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins/SJ.out.tab | \
awk 'BEGIN {OFS="	"; strChar[0]="."; strChar[1]="+"; strChar[2]="-"} {if($5>0){print $1,$2,$3,strChar[$4]}}' | sort -k1,1h -k2,2n > alignment_1stPass/AllSamples.SJ.out.tab && \
mkdir -p reference.Merged && \
STAR --runMode genomeGenerate \
  --genomeDir reference.Merged \
  --genomeFastaFiles /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  --runThreadN 16 \
  --limitGenomeGenerateRAM 100000000000 \
  --sjdbFileChrStartEnd alignment_1stPass/AllSamples.SJ.out.tab \
  --limitIObufferSize 1000000000 \
  --sjdbOverhang 99
star_index.AllSamples.2675d30f15f8530d87b4c9778528e61d.mugqic.done
)
star_7_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=15:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_8_JOB_ID: star_align.2.HI.4519.002.Index_1.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.HI.4519.002.Index_1.CTRL
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$star_7_JOB_ID
JOB_DONE=job_output/star/star_align.2.HI.4519.002.Index_1.CTRL.c7c1d62697ccb542ee3fbd3acff215f1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.HI.4519.002.Index_1.CTRL.c7c1d62697ccb542ee3fbd3acff215f1.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/CTRL/HI.4519.002.Index_1.CTRL && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/CTRL/HI.4519.002.Index_1.CTRL.trim.pair1.fastq.gz \
    trim/CTRL/HI.4519.002.Index_1.CTRL.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/CTRL/HI.4519.002.Index_1.CTRL/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_1.CTRL" 	PL:"ILLUMINA" 			SM:"CTRL" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f HI.4519.002.Index_1.CTRL/Aligned.sortedByCoord.out.bam alignment/CTRL/CTRL.sorted.bam
star_align.2.HI.4519.002.Index_1.CTRL.c7c1d62697ccb542ee3fbd3acff215f1.mugqic.done
)
star_8_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_9_JOB_ID: star_align.2.HI.4519.002.Index_2.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.HI.4519.002.Index_2.L-FAT
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID:$star_7_JOB_ID
JOB_DONE=job_output/star/star_align.2.HI.4519.002.Index_2.L-FAT.a5e26f5288f99a7c39cf91f9532923c3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.HI.4519.002.Index_2.L-FAT.a5e26f5288f99a7c39cf91f9532923c3.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/L-FAT/HI.4519.002.Index_2.L-FAT && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.pair1.fastq.gz \
    trim/L-FAT/HI.4519.002.Index_2.L-FAT.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/L-FAT/HI.4519.002.Index_2.L-FAT/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_2.L-FAT" 	PL:"ILLUMINA" 			SM:"L-FAT" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f HI.4519.002.Index_2.L-FAT/Aligned.sortedByCoord.out.bam alignment/L-FAT/L-FAT.sorted.bam
star_align.2.HI.4519.002.Index_2.L-FAT.a5e26f5288f99a7c39cf91f9532923c3.mugqic.done
)
star_9_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_10_JOB_ID: star_align.2.HI.4519.002.Index_3.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.HI.4519.002.Index_3.H-FAT
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID:$star_7_JOB_ID
JOB_DONE=job_output/star/star_align.2.HI.4519.002.Index_3.H-FAT.9ad0506e96737ba2270803ec819ce0c4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.HI.4519.002.Index_3.H-FAT.9ad0506e96737ba2270803ec819ce0c4.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/H-FAT/HI.4519.002.Index_3.H-FAT && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.pair1.fastq.gz \
    trim/H-FAT/HI.4519.002.Index_3.H-FAT.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/H-FAT/HI.4519.002.Index_3.H-FAT/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_3.H-FAT" 	PL:"ILLUMINA" 			SM:"H-FAT" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f HI.4519.002.Index_3.H-FAT/Aligned.sortedByCoord.out.bam alignment/H-FAT/H-FAT.sorted.bam
star_align.2.HI.4519.002.Index_3.H-FAT.9ad0506e96737ba2270803ec819ce0c4.mugqic.done
)
star_10_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_11_JOB_ID: star_align.2.HI.4519.002.Index_4.CTRL-Ins
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.HI.4519.002.Index_4.CTRL-Ins
JOB_DEPENDENCIES=$trimmomatic_4_JOB_ID:$star_7_JOB_ID
JOB_DONE=job_output/star/star_align.2.HI.4519.002.Index_4.CTRL-Ins.aa31c5cafdae55ae582d2c8355d79370.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.HI.4519.002.Index_4.CTRL-Ins.aa31c5cafdae55ae582d2c8355d79370.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.pair1.fastq.gz \
    trim/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_4.CTRL-Ins" 	PL:"ILLUMINA" 			SM:"CTRL.Ins" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f HI.4519.002.Index_4.CTRL-Ins/Aligned.sortedByCoord.out.bam alignment/CTRL.Ins/CTRL.Ins.sorted.bam
star_align.2.HI.4519.002.Index_4.CTRL-Ins.aa31c5cafdae55ae582d2c8355d79370.mugqic.done
)
star_11_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_12_JOB_ID: star_align.2.HI.4519.002.Index_5.L-FAT-Ins
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.HI.4519.002.Index_5.L-FAT-Ins
JOB_DEPENDENCIES=$trimmomatic_5_JOB_ID:$star_7_JOB_ID
JOB_DONE=job_output/star/star_align.2.HI.4519.002.Index_5.L-FAT-Ins.6cbebe92d81ba5de11a055a029f65203.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.HI.4519.002.Index_5.L-FAT-Ins.6cbebe92d81ba5de11a055a029f65203.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.pair1.fastq.gz \
    trim/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_5.L-FAT-Ins" 	PL:"ILLUMINA" 			SM:"L-FAT.Ins" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f HI.4519.002.Index_5.L-FAT-Ins/Aligned.sortedByCoord.out.bam alignment/L-FAT.Ins/L-FAT.Ins.sorted.bam
star_align.2.HI.4519.002.Index_5.L-FAT-Ins.6cbebe92d81ba5de11a055a029f65203.mugqic.done
)
star_12_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_13_JOB_ID: star_align.2.HI.4519.002.Index_6.H-FAT-Ins
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.HI.4519.002.Index_6.H-FAT-Ins
JOB_DEPENDENCIES=$trimmomatic_6_JOB_ID:$star_7_JOB_ID
JOB_DONE=job_output/star/star_align.2.HI.4519.002.Index_6.H-FAT-Ins.4f6cd6b0c30a95f255b8c0951d78cbdb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.HI.4519.002.Index_6.H-FAT-Ins.4f6cd6b0c30a95f255b8c0951d78cbdb.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.pair1.fastq.gz \
    trim/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins.trim.pair2.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins/ \
  --outSAMattrRGline ID:"HI.4519.002.Index_6.H-FAT-Ins" 	PL:"ILLUMINA" 			SM:"H-FAT.Ins" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f HI.4519.002.Index_6.H-FAT-Ins/Aligned.sortedByCoord.out.bam alignment/H-FAT.Ins/H-FAT.Ins.sorted.bam
star_align.2.HI.4519.002.Index_6.H-FAT-Ins.4f6cd6b0c30a95f255b8c0951d78cbdb.mugqic.done
)
star_13_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: star_14_JOB_ID: star_report
#-------------------------------------------------------------------------------
JOB_NAME=star_report
JOB_DEPENDENCIES=$star_8_JOB_ID:$star_9_JOB_ID:$star_10_JOB_ID:$star_11_JOB_ID:$star_12_JOB_ID:$star_13_JOB_ID
JOB_DONE=job_output/star/star_report.20b949a56558f0d07a6baecd0297fd46.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_report.20b949a56558f0d07a6baecd0297fd46.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.star.md \
  --variable scientific_name="Homo_sapiens" \
  --variable assembly="GRCh38" \
  /home/efournie/genpipes/bfx/report/RnaSeq.star.md \
  > report/RnaSeq.star.md
star_report.20b949a56558f0d07a6baecd0297fd46.mugqic.done
)
star_14_JOB_ID=$(echo "rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_sort_sam
#-------------------------------------------------------------------------------
STEP=picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_1_JOB_ID: picard_sort_sam.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.CTRL
JOB_DEPENDENCIES=$star_8_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.CTRL.d9f0f57ec24311570e7a980758dd0c64.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.CTRL.d9f0f57ec24311570e7a980758dd0c64.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL/CTRL.sorted.bam \
 OUTPUT=alignment/CTRL/CTRL.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.CTRL.d9f0f57ec24311570e7a980758dd0c64.mugqic.done
)
picard_sort_sam_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_2_JOB_ID: picard_sort_sam.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.L-FAT
JOB_DEPENDENCIES=$star_9_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.L-FAT.48ade0192b466acdf8c9e80dcfb9dcb2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.L-FAT.48ade0192b466acdf8c9e80dcfb9dcb2.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT/L-FAT.sorted.bam \
 OUTPUT=alignment/L-FAT/L-FAT.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.L-FAT.48ade0192b466acdf8c9e80dcfb9dcb2.mugqic.done
)
picard_sort_sam_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_3_JOB_ID: picard_sort_sam.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.H-FAT
JOB_DEPENDENCIES=$star_10_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.H-FAT.b9d179aa521a5f4171ade9cc4a3f69cb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.H-FAT.b9d179aa521a5f4171ade9cc4a3f69cb.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT/H-FAT.sorted.bam \
 OUTPUT=alignment/H-FAT/H-FAT.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.H-FAT.b9d179aa521a5f4171ade9cc4a3f69cb.mugqic.done
)
picard_sort_sam_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_4_JOB_ID: picard_sort_sam.CTRL.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.CTRL.Ins
JOB_DEPENDENCIES=$star_11_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.CTRL.Ins.267807b4fdd6ec47840c4f0357d44932.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.CTRL.Ins.267807b4fdd6ec47840c4f0357d44932.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.bam \
 OUTPUT=alignment/CTRL.Ins/CTRL.Ins.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.CTRL.Ins.267807b4fdd6ec47840c4f0357d44932.mugqic.done
)
picard_sort_sam_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_5_JOB_ID: picard_sort_sam.L-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.L-FAT.Ins
JOB_DEPENDENCIES=$star_12_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.L-FAT.Ins.5859df209c6f91044d44c73cb3571331.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.L-FAT.Ins.5859df209c6f91044d44c73cb3571331.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.bam \
 OUTPUT=alignment/L-FAT.Ins/L-FAT.Ins.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.L-FAT.Ins.5859df209c6f91044d44c73cb3571331.mugqic.done
)
picard_sort_sam_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_6_JOB_ID: picard_sort_sam.H-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.H-FAT.Ins
JOB_DEPENDENCIES=$star_13_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.H-FAT.Ins.ab931d83e305dddbeab0f4706f755b06.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.H-FAT.Ins.ab931d83e305dddbeab0f4706f755b06.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.bam \
 OUTPUT=alignment/H-FAT.Ins/H-FAT.Ins.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.H-FAT.Ins.ab931d83e305dddbeab0f4706f755b06.mugqic.done
)
picard_sort_sam_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.CTRL
JOB_DEPENDENCIES=$star_8_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.CTRL.6e6616e2c8686a93795a129a63b7a4f1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.CTRL.6e6616e2c8686a93795a129a63b7a4f1.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL/CTRL.sorted.bam \
 OUTPUT=alignment/CTRL/CTRL.sorted.mdup.bam \
 METRICS_FILE=alignment/CTRL/CTRL.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.CTRL.6e6616e2c8686a93795a129a63b7a4f1.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.L-FAT
JOB_DEPENDENCIES=$star_9_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.L-FAT.764616232be31ea151f03b4f9679f93c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.L-FAT.764616232be31ea151f03b4f9679f93c.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT/L-FAT.sorted.bam \
 OUTPUT=alignment/L-FAT/L-FAT.sorted.mdup.bam \
 METRICS_FILE=alignment/L-FAT/L-FAT.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.L-FAT.764616232be31ea151f03b4f9679f93c.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_3_JOB_ID: picard_mark_duplicates.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.H-FAT
JOB_DEPENDENCIES=$star_10_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.H-FAT.d895e73e006e5fb7f5dba1cee252cb15.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.H-FAT.d895e73e006e5fb7f5dba1cee252cb15.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT/H-FAT.sorted.bam \
 OUTPUT=alignment/H-FAT/H-FAT.sorted.mdup.bam \
 METRICS_FILE=alignment/H-FAT/H-FAT.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.H-FAT.d895e73e006e5fb7f5dba1cee252cb15.mugqic.done
)
picard_mark_duplicates_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_4_JOB_ID: picard_mark_duplicates.CTRL.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.CTRL.Ins
JOB_DEPENDENCIES=$star_11_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.CTRL.Ins.e0a8210d87c46be78e7ae27fbaa1c6f2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.CTRL.Ins.e0a8210d87c46be78e7ae27fbaa1c6f2.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.bam \
 OUTPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam \
 METRICS_FILE=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.CTRL.Ins.e0a8210d87c46be78e7ae27fbaa1c6f2.mugqic.done
)
picard_mark_duplicates_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_5_JOB_ID: picard_mark_duplicates.L-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.L-FAT.Ins
JOB_DEPENDENCIES=$star_12_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.L-FAT.Ins.a784deffb960ed869ee1cc5ce381d3c1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.L-FAT.Ins.a784deffb960ed869ee1cc5ce381d3c1.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.bam \
 OUTPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam \
 METRICS_FILE=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.L-FAT.Ins.a784deffb960ed869ee1cc5ce381d3c1.mugqic.done
)
picard_mark_duplicates_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_6_JOB_ID: picard_mark_duplicates.H-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.H-FAT.Ins
JOB_DEPENDENCIES=$star_13_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.H-FAT.Ins.124b5e0c112c2bdbc80997862083630c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.H-FAT.Ins.124b5e0c112c2bdbc80997862083630c.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.bam \
 OUTPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam \
 METRICS_FILE=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.H-FAT.Ins.124b5e0c112c2bdbc80997862083630c.mugqic.done
)
picard_mark_duplicates_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_rna_metrics
#-------------------------------------------------------------------------------
STEP=picard_rna_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_1_JOB_ID: picard_rna_metrics.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.CTRL
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.CTRL.cf26e1ce70aa41eb2b36861561b22000.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.CTRL.cf26e1ce70aa41eb2b36861561b22000.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/CTRL && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 INPUT=alignment/CTRL/CTRL.sorted.mdup.bam \
 OUTPUT=metrics/CTRL/CTRL \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL/CTRL.sorted.mdup.bam \
 OUTPUT=metrics/CTRL/CTRL.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.CTRL.cf26e1ce70aa41eb2b36861561b22000.mugqic.done
)
picard_rna_metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_2_JOB_ID: picard_rna_metrics.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.L-FAT
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.L-FAT.9f7c19694178937f4a8821e8760df637.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.L-FAT.9f7c19694178937f4a8821e8760df637.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/L-FAT && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 INPUT=alignment/L-FAT/L-FAT.sorted.mdup.bam \
 OUTPUT=metrics/L-FAT/L-FAT \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT/L-FAT.sorted.mdup.bam \
 OUTPUT=metrics/L-FAT/L-FAT.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.L-FAT.9f7c19694178937f4a8821e8760df637.mugqic.done
)
picard_rna_metrics_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_3_JOB_ID: picard_rna_metrics.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.H-FAT
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.H-FAT.faa734f313cfbc98f381a3cbe596ba96.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.H-FAT.faa734f313cfbc98f381a3cbe596ba96.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/H-FAT && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 INPUT=alignment/H-FAT/H-FAT.sorted.mdup.bam \
 OUTPUT=metrics/H-FAT/H-FAT \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT/H-FAT.sorted.mdup.bam \
 OUTPUT=metrics/H-FAT/H-FAT.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.H-FAT.faa734f313cfbc98f381a3cbe596ba96.mugqic.done
)
picard_rna_metrics_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_4_JOB_ID: picard_rna_metrics.CTRL.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.CTRL.Ins
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.CTRL.Ins.76c57778a8bfc2434d1958965653c269.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.CTRL.Ins.76c57778a8bfc2434d1958965653c269.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/CTRL.Ins && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 INPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam \
 OUTPUT=metrics/CTRL.Ins/CTRL.Ins \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam \
 OUTPUT=metrics/CTRL.Ins/CTRL.Ins.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.CTRL.Ins.76c57778a8bfc2434d1958965653c269.mugqic.done
)
picard_rna_metrics_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_5_JOB_ID: picard_rna_metrics.L-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.L-FAT.Ins
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.L-FAT.Ins.eba1240c403bc81f5f42b6529a22bbf9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.L-FAT.Ins.eba1240c403bc81f5f42b6529a22bbf9.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/L-FAT.Ins && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 INPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam \
 OUTPUT=metrics/L-FAT.Ins/L-FAT.Ins \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam \
 OUTPUT=metrics/L-FAT.Ins/L-FAT.Ins.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.L-FAT.Ins.eba1240c403bc81f5f42b6529a22bbf9.mugqic.done
)
picard_rna_metrics_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_6_JOB_ID: picard_rna_metrics.H-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.H-FAT.Ins
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.H-FAT.Ins.ea1277be72ed0a1bdae36c3a28bd33cf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.H-FAT.Ins.ea1277be72ed0a1bdae36c3a28bd33cf.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/H-FAT.Ins && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 INPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam \
 OUTPUT=metrics/H-FAT.Ins/H-FAT.Ins \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam \
 OUTPUT=metrics/H-FAT.Ins/H-FAT.Ins.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.H-FAT.Ins.ea1277be72ed0a1bdae36c3a28bd33cf.mugqic.done
)
picard_rna_metrics_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: estimate_ribosomal_rna
#-------------------------------------------------------------------------------
STEP=estimate_ribosomal_rna
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_1_JOB_ID: bwa_mem_rRNA.HI.4519.002.Index_1.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.HI.4519.002.Index_1.CTRL
JOB_DEPENDENCIES=$star_8_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.HI.4519.002.Index_1.CTRL.aa2d162b6311a025aacdc913eeca341a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.HI.4519.002.Index_1.CTRL.aa2d162b6311a025aacdc913eeca341a.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/CTRL/HI.4519.002.Index_1.CTRL metrics/CTRL/HI.4519.002.Index_1.CTRL && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/CTRL/HI.4519.002.Index_1.CTRL/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:HI.4519.002.Index_1.CTRL	SM:CTRL	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl87.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/CTRL/HI.4519.002.Index_1.CTRL/HI.4519.002.Index_1.CTRLrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/CTRL/HI.4519.002.Index_1.CTRL/HI.4519.002.Index_1.CTRLrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  -o metrics/CTRL/HI.4519.002.Index_1.CTRL/HI.4519.002.Index_1.CTRLrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.HI.4519.002.Index_1.CTRL.aa2d162b6311a025aacdc913eeca341a.mugqic.done
)
estimate_ribosomal_rna_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_2_JOB_ID: bwa_mem_rRNA.HI.4519.002.Index_2.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.HI.4519.002.Index_2.L-FAT
JOB_DEPENDENCIES=$star_9_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.HI.4519.002.Index_2.L-FAT.2e2f2cfeb3df4681e1d81553e6f6515a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.HI.4519.002.Index_2.L-FAT.2e2f2cfeb3df4681e1d81553e6f6515a.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/L-FAT/HI.4519.002.Index_2.L-FAT metrics/L-FAT/HI.4519.002.Index_2.L-FAT && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/L-FAT/HI.4519.002.Index_2.L-FAT/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:HI.4519.002.Index_2.L-FAT	SM:L-FAT	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl87.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/L-FAT/HI.4519.002.Index_2.L-FAT/HI.4519.002.Index_2.L-FATrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/L-FAT/HI.4519.002.Index_2.L-FAT/HI.4519.002.Index_2.L-FATrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  -o metrics/L-FAT/HI.4519.002.Index_2.L-FAT/HI.4519.002.Index_2.L-FATrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.HI.4519.002.Index_2.L-FAT.2e2f2cfeb3df4681e1d81553e6f6515a.mugqic.done
)
estimate_ribosomal_rna_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_3_JOB_ID: bwa_mem_rRNA.HI.4519.002.Index_3.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.HI.4519.002.Index_3.H-FAT
JOB_DEPENDENCIES=$star_10_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.HI.4519.002.Index_3.H-FAT.176b2f4208422d49616f17586ae48dfe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.HI.4519.002.Index_3.H-FAT.176b2f4208422d49616f17586ae48dfe.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/H-FAT/HI.4519.002.Index_3.H-FAT metrics/H-FAT/HI.4519.002.Index_3.H-FAT && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/H-FAT/HI.4519.002.Index_3.H-FAT/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:HI.4519.002.Index_3.H-FAT	SM:H-FAT	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl87.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/H-FAT/HI.4519.002.Index_3.H-FAT/HI.4519.002.Index_3.H-FATrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/H-FAT/HI.4519.002.Index_3.H-FAT/HI.4519.002.Index_3.H-FATrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  -o metrics/H-FAT/HI.4519.002.Index_3.H-FAT/HI.4519.002.Index_3.H-FATrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.HI.4519.002.Index_3.H-FAT.176b2f4208422d49616f17586ae48dfe.mugqic.done
)
estimate_ribosomal_rna_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_4_JOB_ID: bwa_mem_rRNA.HI.4519.002.Index_4.CTRL-Ins
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.HI.4519.002.Index_4.CTRL-Ins
JOB_DEPENDENCIES=$star_11_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.HI.4519.002.Index_4.CTRL-Ins.12f873a6eec2c51278d0bfa95022fc7c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.HI.4519.002.Index_4.CTRL-Ins.12f873a6eec2c51278d0bfa95022fc7c.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins metrics/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:HI.4519.002.Index_4.CTRL-Ins	SM:CTRL.Ins	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl87.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins/HI.4519.002.Index_4.CTRL-InsrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins/HI.4519.002.Index_4.CTRL-InsrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  -o metrics/CTRL.Ins/HI.4519.002.Index_4.CTRL-Ins/HI.4519.002.Index_4.CTRL-InsrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.HI.4519.002.Index_4.CTRL-Ins.12f873a6eec2c51278d0bfa95022fc7c.mugqic.done
)
estimate_ribosomal_rna_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_5_JOB_ID: bwa_mem_rRNA.HI.4519.002.Index_5.L-FAT-Ins
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.HI.4519.002.Index_5.L-FAT-Ins
JOB_DEPENDENCIES=$star_12_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.HI.4519.002.Index_5.L-FAT-Ins.bdca863f91ad2f0b9cf324798dfc0f4b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.HI.4519.002.Index_5.L-FAT-Ins.bdca863f91ad2f0b9cf324798dfc0f4b.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins metrics/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:HI.4519.002.Index_5.L-FAT-Ins	SM:L-FAT.Ins	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl87.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins/HI.4519.002.Index_5.L-FAT-InsrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins/HI.4519.002.Index_5.L-FAT-InsrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  -o metrics/L-FAT.Ins/HI.4519.002.Index_5.L-FAT-Ins/HI.4519.002.Index_5.L-FAT-InsrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.HI.4519.002.Index_5.L-FAT-Ins.bdca863f91ad2f0b9cf324798dfc0f4b.mugqic.done
)
estimate_ribosomal_rna_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_6_JOB_ID: bwa_mem_rRNA.HI.4519.002.Index_6.H-FAT-Ins
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.HI.4519.002.Index_6.H-FAT-Ins
JOB_DEPENDENCIES=$star_13_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.HI.4519.002.Index_6.H-FAT-Ins.d506e296710d0b75cb69d473718846b1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.HI.4519.002.Index_6.H-FAT-Ins.d506e296710d0b75cb69d473718846b1.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins metrics/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:HI.4519.002.Index_6.H-FAT-Ins	SM:H-FAT.Ins	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl87.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins/HI.4519.002.Index_6.H-FAT-InsrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins/HI.4519.002.Index_6.H-FAT-InsrRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  -o metrics/H-FAT.Ins/HI.4519.002.Index_6.H-FAT-Ins/HI.4519.002.Index_6.H-FAT-InsrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.HI.4519.002.Index_6.H-FAT-Ins.d506e296710d0b75cb69d473718846b1.mugqic.done
)
estimate_ribosomal_rna_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: bam_hard_clip
#-------------------------------------------------------------------------------
STEP=bam_hard_clip
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_1_JOB_ID: tuxedo_hard_clip.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.CTRL
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.CTRL.8dbb7f09440eb53f122c2f1a893e405a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.CTRL.8dbb7f09440eb53f122c2f1a893e405a.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/CTRL/CTRL.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/CTRL/CTRL.sorted.mdup.hardClip.bam
tuxedo_hard_clip.CTRL.8dbb7f09440eb53f122c2f1a893e405a.mugqic.done
)
bam_hard_clip_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_2_JOB_ID: tuxedo_hard_clip.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.L-FAT
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.L-FAT.11553cf1ae362a23e67647ceeb62b666.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.L-FAT.11553cf1ae362a23e67647ceeb62b666.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/L-FAT/L-FAT.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/L-FAT/L-FAT.sorted.mdup.hardClip.bam
tuxedo_hard_clip.L-FAT.11553cf1ae362a23e67647ceeb62b666.mugqic.done
)
bam_hard_clip_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_3_JOB_ID: tuxedo_hard_clip.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.H-FAT
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.H-FAT.af60ad9ab9b6f8b05879a233c26a4135.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.H-FAT.af60ad9ab9b6f8b05879a233c26a4135.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/H-FAT/H-FAT.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/H-FAT/H-FAT.sorted.mdup.hardClip.bam
tuxedo_hard_clip.H-FAT.af60ad9ab9b6f8b05879a233c26a4135.mugqic.done
)
bam_hard_clip_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_4_JOB_ID: tuxedo_hard_clip.CTRL.Ins
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.CTRL.Ins
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.CTRL.Ins.098faf1878dd797e47f4599e21d535e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.CTRL.Ins.098faf1878dd797e47f4599e21d535e8.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.hardClip.bam
tuxedo_hard_clip.CTRL.Ins.098faf1878dd797e47f4599e21d535e8.mugqic.done
)
bam_hard_clip_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_5_JOB_ID: tuxedo_hard_clip.L-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.L-FAT.Ins
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.L-FAT.Ins.c2ac697dfa56ae0bf89911c20e3a3000.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.L-FAT.Ins.c2ac697dfa56ae0bf89911c20e3a3000.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.hardClip.bam
tuxedo_hard_clip.L-FAT.Ins.c2ac697dfa56ae0bf89911c20e3a3000.mugqic.done
)
bam_hard_clip_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_6_JOB_ID: tuxedo_hard_clip.H-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.H-FAT.Ins
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.H-FAT.Ins.ca3d00b0c635ed0affee7e6bd213f4c3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.H-FAT.Ins.ca3d00b0c635ed0affee7e6bd213f4c3.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.hardClip.bam
tuxedo_hard_clip.H-FAT.Ins.ca3d00b0c635ed0affee7e6bd213f4c3.mugqic.done
)
bam_hard_clip_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: rnaseqc
#-------------------------------------------------------------------------------
STEP=rnaseqc
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: rnaseqc_1_JOB_ID: rnaseqc
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc.bd60b389207cc7074eec1e6af1fb6598.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc.bd60b389207cc7074eec1e6af1fb6598.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bwa/0.7.15 mugqic/rnaseqc/1.1.8 && \
mkdir -p metrics/rnaseqRep && \
echo "Sample	BamFile	Note
CTRL	alignment/CTRL/CTRL.sorted.mdup.bam	RNAseq
L-FAT	alignment/L-FAT/L-FAT.sorted.mdup.bam	RNAseq
H-FAT	alignment/H-FAT/H-FAT.sorted.mdup.bam	RNAseq
CTRL.Ins	alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam	RNAseq
L-FAT.Ins	alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam	RNAseq
H-FAT.Ins	alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam	RNAseq" \
  > alignment/rnaseqc.samples.txt && \
touch dummy_rRNA.fa && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $RNASEQC_JAR \
  -n 1000 \
  -o metrics/rnaseqRep \
  -r /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  -s alignment/rnaseqc.samples.txt \
  -t /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.transcript_id.gtf \
  -ttype 2\
  -BWArRNA dummy_rRNA.fa && \
zip -r metrics/rnaseqRep.zip metrics/rnaseqRep
rnaseqc.bd60b389207cc7074eec1e6af1fb6598.mugqic.done
)
rnaseqc_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"rnaseqc\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"rnaseqc\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=72:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$rnaseqc_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: rnaseqc_2_JOB_ID: rnaseqc_report
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done'
module load mugqic/python/2.7.13 mugqic/pandoc/1.15.2 && \
mkdir -p report && \
cp metrics/rnaseqRep.zip report/reportRNAseqQC.zip && \
python -c 'import csv; csv_in = csv.DictReader(open("metrics/rnaseqRep/metrics.tsv"), delimiter="	")
print "	".join(["Sample", "Aligned Reads", "Alternative Alignments", "%", "rRNA Reads", "Coverage", "Exonic Rate", "Genes"])
print "\n".join(["	".join([
    line["Sample"],
    line["Mapped"],
    line["Alternative Aligments"],
    str(float(line["Alternative Aligments"]) / float(line["Mapped"]) * 100),
    line["rRNA"],
    line["Mean Per Base Cov."],
    line["Exonic Rate"],
    line["Genes Detected"]
]) for line in csv_in])' \
  > report/trimAlignmentTable.tsv.tmp && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F"	" 'FNR==NR{raw_reads[$1]=$2; surviving_reads[$1]=$3; surviving_pct[$1]=$4; next}{OFS="	"; if ($2=="Aligned Reads"){surviving_pct[$1]="%"; aligned_pct="%"; rrna_pct="%"} else {aligned_pct=($2 / surviving_reads[$1] * 100); rrna_pct=($5 / surviving_reads[$1] * 100)}; printf $1"	"raw_reads[$1]"	"surviving_reads[$1]"	"surviving_pct[$1]"	"$2"	"aligned_pct"	"$3"	"$4"	"$5"	"rrna_pct; for (i = 6; i<= NF; i++) {printf "	"$i}; print ""}' \
  metrics/trimSampleTable.tsv \
  report/trimAlignmentTable.tsv.tmp \
  > report/trimAlignmentTable.tsv
else
  cp report/trimAlignmentTable.tsv.tmp report/trimAlignmentTable.tsv
fi && \
rm report/trimAlignmentTable.tsv.tmp && \
trim_alignment_table_md=`if [[ -f metrics/trimSampleTable.tsv ]] ; then cut -f1-13 report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8), sprintf("%\47d", $9), sprintf("%.1f", $10), sprintf("%.2f", $11), sprintf("%.2f", $12), sprintf("%\47d", $13)}}' ; else cat report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.2f", $6), sprintf("%.2f", $7), $8}}' ; fi`
pandoc \
  /home/efournie/genpipes/bfx/report/RnaSeq.rnaseqc.md \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.rnaseqc.md \
  --variable trim_alignment_table="$trim_alignment_table_md" \
  --to markdown \
  > report/RnaSeq.rnaseqc.md
rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done
)
rnaseqc_2_JOB_ID=$(echo "rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$rnaseqc_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: wiggle
#-------------------------------------------------------------------------------
STEP=wiggle
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: wiggle_1_JOB_ID: wiggle.CTRL.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.CTRL.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.CTRL.forward_strandspec.34ca1ea90a1792c6c03c1aed7f0fb0f8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.CTRL.forward_strandspec.34ca1ea90a1792c6c03c1aed7f0fb0f8.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
samtools view -bh -F 256 -f 81 \
  alignment/CTRL/CTRL.sorted.mdup.bam \
  > alignment/CTRL/CTRL.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/CTRL/CTRL.sorted.mdup.bam \
  > alignment/CTRL/CTRL.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL/CTRL.sorted.mdup.tmp1.forward.bam \
 INPUT=alignment/CTRL/CTRL.sorted.mdup.tmp2.forward.bam \
 OUTPUT=alignment/CTRL/CTRL.sorted.mdup.forward.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/CTRL/CTRL.sorted.mdup.tmp1.forward.bam alignment/CTRL/CTRL.sorted.mdup.tmp2.forward.bam
wiggle.CTRL.forward_strandspec.34ca1ea90a1792c6c03c1aed7f0fb0f8.mugqic.done
)
wiggle_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_2_JOB_ID: wiggle.CTRL.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.CTRL.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.CTRL.reverse_strandspec.dbc2dcc68b3ce6cfafbcb0afd92adb37.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.CTRL.reverse_strandspec.dbc2dcc68b3ce6cfafbcb0afd92adb37.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
mkdir -p tracks/CTRL tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/CTRL/CTRL.sorted.mdup.bam \
  > alignment/CTRL/CTRL.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/CTRL/CTRL.sorted.mdup.bam \
  > alignment/CTRL/CTRL.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL/CTRL.sorted.mdup.tmp1.reverse.bam \
 INPUT=alignment/CTRL/CTRL.sorted.mdup.tmp2.reverse.bam \
 OUTPUT=alignment/CTRL/CTRL.sorted.mdup.reverse.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/CTRL/CTRL.sorted.mdup.tmp1.reverse.bam alignment/CTRL/CTRL.sorted.mdup.tmp2.reverse.bam
wiggle.CTRL.reverse_strandspec.dbc2dcc68b3ce6cfafbcb0afd92adb37.mugqic.done
)
wiggle_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_3_JOB_ID: bed_graph.CTRL.forward
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.CTRL.forward
JOB_DEPENDENCIES=$wiggle_1_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.CTRL.forward.7c30f5aa0cd9ea6be14a94428ef267e1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.CTRL.forward.7c30f5aa0cd9ea6be14a94428ef267e1.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/CTRL  && \
nmblines=$(samtools view -F 256 -f 81  alignment/CTRL/CTRL.sorted.mdup.forward.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/CTRL/CTRL.sorted.mdup.forward.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/CTRL/CTRL.forward.bedGraph
bed_graph.CTRL.forward.7c30f5aa0cd9ea6be14a94428ef267e1.mugqic.done
)
wiggle_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_4_JOB_ID: wiggle.CTRL.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.CTRL.forward
JOB_DEPENDENCIES=$wiggle_3_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.CTRL.forward.d4ed9854a82a54d695bb4b6e9f791dc4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.CTRL.forward.d4ed9854a82a54d695bb4b6e9f791dc4.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/CTRL/CTRL.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/CTRL/CTRL.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/CTRL/CTRL.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/CTRL.forward.bw
wiggle.CTRL.forward.d4ed9854a82a54d695bb4b6e9f791dc4.mugqic.done
)
wiggle_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_5_JOB_ID: bed_graph.CTRL.reverse
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.CTRL.reverse
JOB_DEPENDENCIES=$wiggle_2_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.CTRL.reverse.4c03a6d64b07c86591455772c2f6feab.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.CTRL.reverse.4c03a6d64b07c86591455772c2f6feab.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/CTRL  && \
nmblines=$(samtools view -F 256 -f 97  alignment/CTRL/CTRL.sorted.mdup.reverse.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/CTRL/CTRL.sorted.mdup.reverse.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/CTRL/CTRL.reverse.bedGraph
bed_graph.CTRL.reverse.4c03a6d64b07c86591455772c2f6feab.mugqic.done
)
wiggle_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_6_JOB_ID: wiggle.CTRL.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.CTRL.reverse
JOB_DEPENDENCIES=$wiggle_5_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.CTRL.reverse.7af515cfb44497d5173115ce565aed0f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.CTRL.reverse.7af515cfb44497d5173115ce565aed0f.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/CTRL/CTRL.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/CTRL/CTRL.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/CTRL/CTRL.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/CTRL.reverse.bw
wiggle.CTRL.reverse.7af515cfb44497d5173115ce565aed0f.mugqic.done
)
wiggle_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_7_JOB_ID: wiggle.L-FAT.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.L-FAT.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.L-FAT.forward_strandspec.1c9d94c2c8a98c88038ae1ba9a421e65.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.L-FAT.forward_strandspec.1c9d94c2c8a98c88038ae1ba9a421e65.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
samtools view -bh -F 256 -f 81 \
  alignment/L-FAT/L-FAT.sorted.mdup.bam \
  > alignment/L-FAT/L-FAT.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/L-FAT/L-FAT.sorted.mdup.bam \
  > alignment/L-FAT/L-FAT.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT/L-FAT.sorted.mdup.tmp1.forward.bam \
 INPUT=alignment/L-FAT/L-FAT.sorted.mdup.tmp2.forward.bam \
 OUTPUT=alignment/L-FAT/L-FAT.sorted.mdup.forward.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/L-FAT/L-FAT.sorted.mdup.tmp1.forward.bam alignment/L-FAT/L-FAT.sorted.mdup.tmp2.forward.bam
wiggle.L-FAT.forward_strandspec.1c9d94c2c8a98c88038ae1ba9a421e65.mugqic.done
)
wiggle_7_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_8_JOB_ID: wiggle.L-FAT.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.L-FAT.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.L-FAT.reverse_strandspec.6cc52e606f59b0ccb9135453240789b3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.L-FAT.reverse_strandspec.6cc52e606f59b0ccb9135453240789b3.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
mkdir -p tracks/L-FAT tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/L-FAT/L-FAT.sorted.mdup.bam \
  > alignment/L-FAT/L-FAT.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/L-FAT/L-FAT.sorted.mdup.bam \
  > alignment/L-FAT/L-FAT.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT/L-FAT.sorted.mdup.tmp1.reverse.bam \
 INPUT=alignment/L-FAT/L-FAT.sorted.mdup.tmp2.reverse.bam \
 OUTPUT=alignment/L-FAT/L-FAT.sorted.mdup.reverse.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/L-FAT/L-FAT.sorted.mdup.tmp1.reverse.bam alignment/L-FAT/L-FAT.sorted.mdup.tmp2.reverse.bam
wiggle.L-FAT.reverse_strandspec.6cc52e606f59b0ccb9135453240789b3.mugqic.done
)
wiggle_8_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_9_JOB_ID: bed_graph.L-FAT.forward
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.L-FAT.forward
JOB_DEPENDENCIES=$wiggle_7_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.L-FAT.forward.2c87c736a130ae424bc488168be99393.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.L-FAT.forward.2c87c736a130ae424bc488168be99393.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/L-FAT  && \
nmblines=$(samtools view -F 256 -f 81  alignment/L-FAT/L-FAT.sorted.mdup.forward.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/L-FAT/L-FAT.sorted.mdup.forward.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/L-FAT/L-FAT.forward.bedGraph
bed_graph.L-FAT.forward.2c87c736a130ae424bc488168be99393.mugqic.done
)
wiggle_9_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_10_JOB_ID: wiggle.L-FAT.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.L-FAT.forward
JOB_DEPENDENCIES=$wiggle_9_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.L-FAT.forward.9ecc10e2e62214f6e867da265daf357d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.L-FAT.forward.9ecc10e2e62214f6e867da265daf357d.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/L-FAT/L-FAT.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/L-FAT/L-FAT.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/L-FAT/L-FAT.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/L-FAT.forward.bw
wiggle.L-FAT.forward.9ecc10e2e62214f6e867da265daf357d.mugqic.done
)
wiggle_10_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_11_JOB_ID: bed_graph.L-FAT.reverse
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.L-FAT.reverse
JOB_DEPENDENCIES=$wiggle_8_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.L-FAT.reverse.34139adea3fd2ade609b07303fab363f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.L-FAT.reverse.34139adea3fd2ade609b07303fab363f.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/L-FAT  && \
nmblines=$(samtools view -F 256 -f 97  alignment/L-FAT/L-FAT.sorted.mdup.reverse.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/L-FAT/L-FAT.sorted.mdup.reverse.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/L-FAT/L-FAT.reverse.bedGraph
bed_graph.L-FAT.reverse.34139adea3fd2ade609b07303fab363f.mugqic.done
)
wiggle_11_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_12_JOB_ID: wiggle.L-FAT.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.L-FAT.reverse
JOB_DEPENDENCIES=$wiggle_11_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.L-FAT.reverse.8f53426c291d4412908a179a2e8ea2be.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.L-FAT.reverse.8f53426c291d4412908a179a2e8ea2be.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/L-FAT/L-FAT.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/L-FAT/L-FAT.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/L-FAT/L-FAT.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/L-FAT.reverse.bw
wiggle.L-FAT.reverse.8f53426c291d4412908a179a2e8ea2be.mugqic.done
)
wiggle_12_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_13_JOB_ID: wiggle.H-FAT.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.H-FAT.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.H-FAT.forward_strandspec.e1b58c2f7d75a04a2b0f16a74d008ebd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.H-FAT.forward_strandspec.e1b58c2f7d75a04a2b0f16a74d008ebd.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
samtools view -bh -F 256 -f 81 \
  alignment/H-FAT/H-FAT.sorted.mdup.bam \
  > alignment/H-FAT/H-FAT.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/H-FAT/H-FAT.sorted.mdup.bam \
  > alignment/H-FAT/H-FAT.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT/H-FAT.sorted.mdup.tmp1.forward.bam \
 INPUT=alignment/H-FAT/H-FAT.sorted.mdup.tmp2.forward.bam \
 OUTPUT=alignment/H-FAT/H-FAT.sorted.mdup.forward.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/H-FAT/H-FAT.sorted.mdup.tmp1.forward.bam alignment/H-FAT/H-FAT.sorted.mdup.tmp2.forward.bam
wiggle.H-FAT.forward_strandspec.e1b58c2f7d75a04a2b0f16a74d008ebd.mugqic.done
)
wiggle_13_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_14_JOB_ID: wiggle.H-FAT.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.H-FAT.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.H-FAT.reverse_strandspec.37c057d7a02f64c0b796eb8a7d1b3497.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.H-FAT.reverse_strandspec.37c057d7a02f64c0b796eb8a7d1b3497.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
mkdir -p tracks/H-FAT tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/H-FAT/H-FAT.sorted.mdup.bam \
  > alignment/H-FAT/H-FAT.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/H-FAT/H-FAT.sorted.mdup.bam \
  > alignment/H-FAT/H-FAT.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT/H-FAT.sorted.mdup.tmp1.reverse.bam \
 INPUT=alignment/H-FAT/H-FAT.sorted.mdup.tmp2.reverse.bam \
 OUTPUT=alignment/H-FAT/H-FAT.sorted.mdup.reverse.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/H-FAT/H-FAT.sorted.mdup.tmp1.reverse.bam alignment/H-FAT/H-FAT.sorted.mdup.tmp2.reverse.bam
wiggle.H-FAT.reverse_strandspec.37c057d7a02f64c0b796eb8a7d1b3497.mugqic.done
)
wiggle_14_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_15_JOB_ID: bed_graph.H-FAT.forward
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.H-FAT.forward
JOB_DEPENDENCIES=$wiggle_13_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.H-FAT.forward.d26fa6ba09a0f551381c50452c9134c6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.H-FAT.forward.d26fa6ba09a0f551381c50452c9134c6.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/H-FAT  && \
nmblines=$(samtools view -F 256 -f 81  alignment/H-FAT/H-FAT.sorted.mdup.forward.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/H-FAT/H-FAT.sorted.mdup.forward.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/H-FAT/H-FAT.forward.bedGraph
bed_graph.H-FAT.forward.d26fa6ba09a0f551381c50452c9134c6.mugqic.done
)
wiggle_15_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_16_JOB_ID: wiggle.H-FAT.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.H-FAT.forward
JOB_DEPENDENCIES=$wiggle_15_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.H-FAT.forward.8570b191ec89a27a556f7ec741323c44.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.H-FAT.forward.8570b191ec89a27a556f7ec741323c44.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/H-FAT/H-FAT.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/H-FAT/H-FAT.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/H-FAT/H-FAT.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/H-FAT.forward.bw
wiggle.H-FAT.forward.8570b191ec89a27a556f7ec741323c44.mugqic.done
)
wiggle_16_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_17_JOB_ID: bed_graph.H-FAT.reverse
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.H-FAT.reverse
JOB_DEPENDENCIES=$wiggle_14_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.H-FAT.reverse.bc4cb8b8a8685092cc24ee059ed3e427.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.H-FAT.reverse.bc4cb8b8a8685092cc24ee059ed3e427.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/H-FAT  && \
nmblines=$(samtools view -F 256 -f 97  alignment/H-FAT/H-FAT.sorted.mdup.reverse.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/H-FAT/H-FAT.sorted.mdup.reverse.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/H-FAT/H-FAT.reverse.bedGraph
bed_graph.H-FAT.reverse.bc4cb8b8a8685092cc24ee059ed3e427.mugqic.done
)
wiggle_17_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_18_JOB_ID: wiggle.H-FAT.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.H-FAT.reverse
JOB_DEPENDENCIES=$wiggle_17_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.H-FAT.reverse.a8d2fb3bec5e6740801ff0634aeee34a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.H-FAT.reverse.a8d2fb3bec5e6740801ff0634aeee34a.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/H-FAT/H-FAT.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/H-FAT/H-FAT.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/H-FAT/H-FAT.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/H-FAT.reverse.bw
wiggle.H-FAT.reverse.a8d2fb3bec5e6740801ff0634aeee34a.mugqic.done
)
wiggle_18_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_19_JOB_ID: wiggle.CTRL.Ins.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.CTRL.Ins.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.CTRL.Ins.forward_strandspec.dadeca0f0b4934f8db147d85b0c38680.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.CTRL.Ins.forward_strandspec.dadeca0f0b4934f8db147d85b0c38680.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
samtools view -bh -F 256 -f 81 \
  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam \
  > alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam \
  > alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp1.forward.bam \
 INPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp2.forward.bam \
 OUTPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.forward.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp1.forward.bam alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp2.forward.bam
wiggle.CTRL.Ins.forward_strandspec.dadeca0f0b4934f8db147d85b0c38680.mugqic.done
)
wiggle_19_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_20_JOB_ID: wiggle.CTRL.Ins.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.CTRL.Ins.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.CTRL.Ins.reverse_strandspec.9810a046d2284572292bcca218831ff5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.CTRL.Ins.reverse_strandspec.9810a046d2284572292bcca218831ff5.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
mkdir -p tracks/CTRL.Ins tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam \
  > alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.bam \
  > alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp1.reverse.bam \
 INPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp2.reverse.bam \
 OUTPUT=alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.reverse.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp1.reverse.bam alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.tmp2.reverse.bam
wiggle.CTRL.Ins.reverse_strandspec.9810a046d2284572292bcca218831ff5.mugqic.done
)
wiggle_20_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_21_JOB_ID: bed_graph.CTRL.Ins.forward
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.CTRL.Ins.forward
JOB_DEPENDENCIES=$wiggle_19_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.CTRL.Ins.forward.9c201a1a382a6c169f82b1f807b036bb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.CTRL.Ins.forward.9c201a1a382a6c169f82b1f807b036bb.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/CTRL.Ins  && \
nmblines=$(samtools view -F 256 -f 81  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.forward.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.forward.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/CTRL.Ins/CTRL.Ins.forward.bedGraph
bed_graph.CTRL.Ins.forward.9c201a1a382a6c169f82b1f807b036bb.mugqic.done
)
wiggle_21_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_22_JOB_ID: wiggle.CTRL.Ins.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.CTRL.Ins.forward
JOB_DEPENDENCIES=$wiggle_21_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.CTRL.Ins.forward.5cb437c1a07bdf56dc4aac5e60065488.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.CTRL.Ins.forward.5cb437c1a07bdf56dc4aac5e60065488.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/CTRL.Ins/CTRL.Ins.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/CTRL.Ins/CTRL.Ins.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/CTRL.Ins/CTRL.Ins.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/CTRL.Ins.forward.bw
wiggle.CTRL.Ins.forward.5cb437c1a07bdf56dc4aac5e60065488.mugqic.done
)
wiggle_22_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_23_JOB_ID: bed_graph.CTRL.Ins.reverse
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.CTRL.Ins.reverse
JOB_DEPENDENCIES=$wiggle_20_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.CTRL.Ins.reverse.d1811ee88c4605f78dfa96468bf37eea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.CTRL.Ins.reverse.d1811ee88c4605f78dfa96468bf37eea.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/CTRL.Ins  && \
nmblines=$(samtools view -F 256 -f 97  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.reverse.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.reverse.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/CTRL.Ins/CTRL.Ins.reverse.bedGraph
bed_graph.CTRL.Ins.reverse.d1811ee88c4605f78dfa96468bf37eea.mugqic.done
)
wiggle_23_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_24_JOB_ID: wiggle.CTRL.Ins.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.CTRL.Ins.reverse
JOB_DEPENDENCIES=$wiggle_23_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.CTRL.Ins.reverse.c58f3ed7af96d4e5cc08553dd9f82ba2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.CTRL.Ins.reverse.c58f3ed7af96d4e5cc08553dd9f82ba2.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/CTRL.Ins/CTRL.Ins.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/CTRL.Ins/CTRL.Ins.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/CTRL.Ins/CTRL.Ins.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/CTRL.Ins.reverse.bw
wiggle.CTRL.Ins.reverse.c58f3ed7af96d4e5cc08553dd9f82ba2.mugqic.done
)
wiggle_24_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_25_JOB_ID: wiggle.L-FAT.Ins.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.L-FAT.Ins.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.L-FAT.Ins.forward_strandspec.246a90c6adc36ea57c7e5c95194890d9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.L-FAT.Ins.forward_strandspec.246a90c6adc36ea57c7e5c95194890d9.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
samtools view -bh -F 256 -f 81 \
  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam \
  > alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam \
  > alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp1.forward.bam \
 INPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp2.forward.bam \
 OUTPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.forward.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp1.forward.bam alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp2.forward.bam
wiggle.L-FAT.Ins.forward_strandspec.246a90c6adc36ea57c7e5c95194890d9.mugqic.done
)
wiggle_25_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_26_JOB_ID: wiggle.L-FAT.Ins.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.L-FAT.Ins.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.L-FAT.Ins.reverse_strandspec.2b0c62abd440c35068af1352bbd3890f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.L-FAT.Ins.reverse_strandspec.2b0c62abd440c35068af1352bbd3890f.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
mkdir -p tracks/L-FAT.Ins tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam \
  > alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.bam \
  > alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp1.reverse.bam \
 INPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp2.reverse.bam \
 OUTPUT=alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.reverse.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp1.reverse.bam alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.tmp2.reverse.bam
wiggle.L-FAT.Ins.reverse_strandspec.2b0c62abd440c35068af1352bbd3890f.mugqic.done
)
wiggle_26_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_27_JOB_ID: bed_graph.L-FAT.Ins.forward
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.L-FAT.Ins.forward
JOB_DEPENDENCIES=$wiggle_25_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.L-FAT.Ins.forward.ed248a312347e132d53d765560559dd9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.L-FAT.Ins.forward.ed248a312347e132d53d765560559dd9.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/L-FAT.Ins  && \
nmblines=$(samtools view -F 256 -f 81  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.forward.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.forward.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/L-FAT.Ins/L-FAT.Ins.forward.bedGraph
bed_graph.L-FAT.Ins.forward.ed248a312347e132d53d765560559dd9.mugqic.done
)
wiggle_27_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_28_JOB_ID: wiggle.L-FAT.Ins.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.L-FAT.Ins.forward
JOB_DEPENDENCIES=$wiggle_27_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.L-FAT.Ins.forward.42fa5798f0b9dd62fb7293531d4105d5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.L-FAT.Ins.forward.42fa5798f0b9dd62fb7293531d4105d5.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/L-FAT.Ins/L-FAT.Ins.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/L-FAT.Ins/L-FAT.Ins.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/L-FAT.Ins/L-FAT.Ins.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/L-FAT.Ins.forward.bw
wiggle.L-FAT.Ins.forward.42fa5798f0b9dd62fb7293531d4105d5.mugqic.done
)
wiggle_28_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_29_JOB_ID: bed_graph.L-FAT.Ins.reverse
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.L-FAT.Ins.reverse
JOB_DEPENDENCIES=$wiggle_26_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.L-FAT.Ins.reverse.c9d37c61bddcfbac128d28a64d79e224.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.L-FAT.Ins.reverse.c9d37c61bddcfbac128d28a64d79e224.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/L-FAT.Ins  && \
nmblines=$(samtools view -F 256 -f 97  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.reverse.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.reverse.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/L-FAT.Ins/L-FAT.Ins.reverse.bedGraph
bed_graph.L-FAT.Ins.reverse.c9d37c61bddcfbac128d28a64d79e224.mugqic.done
)
wiggle_29_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_30_JOB_ID: wiggle.L-FAT.Ins.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.L-FAT.Ins.reverse
JOB_DEPENDENCIES=$wiggle_29_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.L-FAT.Ins.reverse.219131bf5af428ebcac26dea64afa6a0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.L-FAT.Ins.reverse.219131bf5af428ebcac26dea64afa6a0.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/L-FAT.Ins/L-FAT.Ins.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/L-FAT.Ins/L-FAT.Ins.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/L-FAT.Ins/L-FAT.Ins.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/L-FAT.Ins.reverse.bw
wiggle.L-FAT.Ins.reverse.219131bf5af428ebcac26dea64afa6a0.mugqic.done
)
wiggle_30_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_31_JOB_ID: wiggle.H-FAT.Ins.forward_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.H-FAT.Ins.forward_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.H-FAT.Ins.forward_strandspec.d68f395681f658edf72696c774a448ea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.H-FAT.Ins.forward_strandspec.d68f395681f658edf72696c774a448ea.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
samtools view -bh -F 256 -f 81 \
  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam \
  > alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp1.forward.bam && \
samtools view -bh -F 256 -f 161 \
  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam \
  > alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp2.forward.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp1.forward.bam \
 INPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp2.forward.bam \
 OUTPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.forward.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp1.forward.bam alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp2.forward.bam
wiggle.H-FAT.Ins.forward_strandspec.d68f395681f658edf72696c774a448ea.mugqic.done
)
wiggle_31_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_32_JOB_ID: wiggle.H-FAT.Ins.reverse_strandspec
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.H-FAT.Ins.reverse_strandspec
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.H-FAT.Ins.reverse_strandspec.f24480b8d78bef587ecdea2ddc6d62a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.H-FAT.Ins.reverse_strandspec.f24480b8d78bef587ecdea2ddc6d62a2.mugqic.done'
module load mugqic/samtools/1.4.1 java/1.8.0_121 mugqic/picard/2.9.0 && \
mkdir -p tracks/H-FAT.Ins tracks/bigWig && \
samtools view -bh -F 256 -f 97 \
  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam \
  > alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp1.reverse.bam && \
samtools view -bh -F 256 -f 145 \
  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.bam \
  > alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp2.reverse.bam && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar MergeSamFiles \
 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp1.reverse.bam \
 INPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp2.reverse.bam \
 OUTPUT=alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.reverse.bam \
 MAX_RECORDS_IN_RAM=5750000 && \
rm alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp1.reverse.bam alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.tmp2.reverse.bam
wiggle.H-FAT.Ins.reverse_strandspec.f24480b8d78bef587ecdea2ddc6d62a2.mugqic.done
)
wiggle_32_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_33_JOB_ID: bed_graph.H-FAT.Ins.forward
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.H-FAT.Ins.forward
JOB_DEPENDENCIES=$wiggle_31_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.H-FAT.Ins.forward.50de300d6dcdbd39ea60d85a155dd9b8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.H-FAT.Ins.forward.50de300d6dcdbd39ea60d85a155dd9b8.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/H-FAT.Ins  && \
nmblines=$(samtools view -F 256 -f 81  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.forward.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.forward.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/H-FAT.Ins/H-FAT.Ins.forward.bedGraph
bed_graph.H-FAT.Ins.forward.50de300d6dcdbd39ea60d85a155dd9b8.mugqic.done
)
wiggle_33_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_34_JOB_ID: wiggle.H-FAT.Ins.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.H-FAT.Ins.forward
JOB_DEPENDENCIES=$wiggle_33_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.H-FAT.Ins.forward.d5b3bf8480764440e63929abe2fdef10.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.H-FAT.Ins.forward.d5b3bf8480764440e63929abe2fdef10.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/H-FAT.Ins/H-FAT.Ins.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/H-FAT.Ins/H-FAT.Ins.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/H-FAT.Ins/H-FAT.Ins.forward.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/H-FAT.Ins.forward.bw
wiggle.H-FAT.Ins.forward.d5b3bf8480764440e63929abe2fdef10.mugqic.done
)
wiggle_34_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_35_JOB_ID: bed_graph.H-FAT.Ins.reverse
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.H-FAT.Ins.reverse
JOB_DEPENDENCIES=$wiggle_32_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.H-FAT.Ins.reverse.c99a5b301e2adda0499b0e330ba5ff95.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.H-FAT.Ins.reverse.c99a5b301e2adda0499b0e330ba5ff95.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/H-FAT.Ins  && \
nmblines=$(samtools view -F 256 -f 97  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.reverse.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.reverse.bam \
  -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  > tracks/H-FAT.Ins/H-FAT.Ins.reverse.bedGraph
bed_graph.H-FAT.Ins.reverse.c99a5b301e2adda0499b0e330ba5ff95.mugqic.done
)
wiggle_35_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: wiggle_36_JOB_ID: wiggle.H-FAT.Ins.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.H-FAT.Ins.reverse
JOB_DEPENDENCIES=$wiggle_35_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.H-FAT.Ins.reverse.28f3f95b5bc6f899746198b7590466bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.H-FAT.Ins.reverse.28f3f95b5bc6f899746198b7590466bd.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/H-FAT.Ins/H-FAT.Ins.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/H-FAT.Ins/H-FAT.Ins.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/H-FAT.Ins/H-FAT.Ins.reverse.bedGraph.sorted \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/bigWig/H-FAT.Ins.reverse.bw
wiggle.H-FAT.Ins.reverse.28f3f95b5bc6f899746198b7590466bd.mugqic.done
)
wiggle_36_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: raw_counts
#-------------------------------------------------------------------------------
STEP=raw_counts
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: raw_counts_1_JOB_ID: htseq_count.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.CTRL
JOB_DEPENDENCIES=$picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.CTRL.65cc431220bd425dee655a2cc8b38852.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.CTRL.65cc431220bd425dee655a2cc8b38852.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/CTRL/CTRL.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  > raw_counts/CTRL.readcounts.csv
htseq_count.CTRL.65cc431220bd425dee655a2cc8b38852.mugqic.done
)
raw_counts_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_2_JOB_ID: htseq_count.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.L-FAT
JOB_DEPENDENCIES=$picard_sort_sam_2_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.L-FAT.84e4ddb86dad021b5f6f935331bab146.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.L-FAT.84e4ddb86dad021b5f6f935331bab146.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/L-FAT/L-FAT.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  > raw_counts/L-FAT.readcounts.csv
htseq_count.L-FAT.84e4ddb86dad021b5f6f935331bab146.mugqic.done
)
raw_counts_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_3_JOB_ID: htseq_count.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.H-FAT
JOB_DEPENDENCIES=$picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.H-FAT.4608cecadaf9963d02e08c4b4ed242a8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.H-FAT.4608cecadaf9963d02e08c4b4ed242a8.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/H-FAT/H-FAT.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  > raw_counts/H-FAT.readcounts.csv
htseq_count.H-FAT.4608cecadaf9963d02e08c4b4ed242a8.mugqic.done
)
raw_counts_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_4_JOB_ID: htseq_count.CTRL.Ins
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.CTRL.Ins
JOB_DEPENDENCIES=$picard_sort_sam_4_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.CTRL.Ins.22e58736c60c7c0796db5be61d0d489b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.CTRL.Ins.22e58736c60c7c0796db5be61d0d489b.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/CTRL.Ins/CTRL.Ins.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  > raw_counts/CTRL.Ins.readcounts.csv
htseq_count.CTRL.Ins.22e58736c60c7c0796db5be61d0d489b.mugqic.done
)
raw_counts_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_5_JOB_ID: htseq_count.L-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.L-FAT.Ins
JOB_DEPENDENCIES=$picard_sort_sam_5_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.L-FAT.Ins.6bd3f5d88c48e0f5fdec2a03794f8f7e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.L-FAT.Ins.6bd3f5d88c48e0f5fdec2a03794f8f7e.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/L-FAT.Ins/L-FAT.Ins.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  > raw_counts/L-FAT.Ins.readcounts.csv
htseq_count.L-FAT.Ins.6bd3f5d88c48e0f5fdec2a03794f8f7e.mugqic.done
)
raw_counts_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_6_JOB_ID: htseq_count.H-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.H-FAT.Ins
JOB_DEPENDENCIES=$picard_sort_sam_6_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.H-FAT.Ins.3d15495dbe7fe451bc714984f0da3951.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.H-FAT.Ins.3d15495dbe7fe451bc714984f0da3951.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/H-FAT.Ins/H-FAT.Ins.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  > raw_counts/H-FAT.Ins.readcounts.csv
htseq_count.H-FAT.Ins.3d15495dbe7fe451bc714984f0da3951.mugqic.done
)
raw_counts_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: raw_counts_metrics
#-------------------------------------------------------------------------------
STEP=raw_counts_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_1_JOB_ID: metrics.matrix
#-------------------------------------------------------------------------------
JOB_NAME=metrics.matrix
JOB_DEPENDENCIES=$raw_counts_1_JOB_ID:$raw_counts_2_JOB_ID:$raw_counts_3_JOB_ID:$raw_counts_4_JOB_ID:$raw_counts_5_JOB_ID:$raw_counts_6_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/metrics.matrix.e5ff9847fef0e742ddc5d109212aacfb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.matrix.e5ff9847fef0e742ddc5d109212aacfb.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 && \
mkdir -p DGE && \
gtf2tmpMatrix.awk \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  DGE/tmpMatrix.txt && \
HEAD='Gene\tSymbol' && \
for read_count_file in \
  raw_counts/CTRL.readcounts.csv \
  raw_counts/L-FAT.readcounts.csv \
  raw_counts/H-FAT.readcounts.csv \
  raw_counts/CTRL.Ins.readcounts.csv \
  raw_counts/L-FAT.Ins.readcounts.csv \
  raw_counts/H-FAT.Ins.readcounts.csv
do
  sort -k1,1 $read_count_file > DGE/tmpSort.txt && \
  join -1 1 -2 1 <(sort -k1,1 DGE/tmpMatrix.txt) DGE/tmpSort.txt > DGE/tmpMatrix.2.txt && \
  mv DGE/tmpMatrix.2.txt DGE/tmpMatrix.txt && \
  na=$(basename $read_count_file | rev | cut -d. -f3- | rev) && \
  HEAD="$HEAD\t$na"
done && \
echo -e $HEAD | cat - DGE/tmpMatrix.txt | tr ' ' '\t' > DGE/rawCountMatrix.csv && \
rm DGE/tmpSort.txt DGE/tmpMatrix.txt
metrics.matrix.e5ff9847fef0e742ddc5d109212aacfb.mugqic.done
)
raw_counts_metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=5:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_2_JOB_ID: metrics.wigzip
#-------------------------------------------------------------------------------
JOB_NAME=metrics.wigzip
JOB_DEPENDENCIES=$wiggle_4_JOB_ID:$wiggle_6_JOB_ID:$wiggle_10_JOB_ID:$wiggle_12_JOB_ID:$wiggle_16_JOB_ID:$wiggle_18_JOB_ID:$wiggle_22_JOB_ID:$wiggle_24_JOB_ID:$wiggle_28_JOB_ID:$wiggle_30_JOB_ID:$wiggle_34_JOB_ID:$wiggle_36_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done'
zip -r tracks.zip tracks/bigWig
metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done
)
raw_counts_metrics_2_JOB_ID=$(echo "rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=5:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_3_JOB_ID: rpkm_saturation
#-------------------------------------------------------------------------------
JOB_NAME=rpkm_saturation
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/rpkm_saturation.631bb01a4e13699cb7d0ca93cf16b425.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rpkm_saturation.631bb01a4e13699cb7d0ca93cf16b425.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 mugqic/mugqic_tools/2.1.9 && \
mkdir -p metrics/saturation && \
Rscript $R_TOOLS/rpkmSaturation.R \
  DGE/rawCountMatrix.csv \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.genes.length.tsv \
  raw_counts \
  metrics/saturation \
  15 \
  1 && \
zip -r metrics/saturation.zip metrics/saturation
rpkm_saturation.631bb01a4e13699cb7d0ca93cf16b425.mugqic.done
)
raw_counts_metrics_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_4_JOB_ID: raw_count_metrics_report
#-------------------------------------------------------------------------------
JOB_NAME=raw_count_metrics_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID:$raw_counts_metrics_2_JOB_ID:$raw_counts_metrics_3_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
cp metrics/rnaseqRep/corrMatrixSpearman.txt report/corrMatrixSpearman.tsv && \
cp tracks.zip report/ && \
cp metrics/saturation.zip report/ && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.raw_counts_metrics.md \
  --variable corr_matrix_spearman_table="`head -16 report/corrMatrixSpearman.tsv | cut -f-16| awk -F"	" '{OFS="	"; if (NR==1) {$0="Vs"$0; print; gsub(/[^	]/, "-"); print} else {printf $1; for (i=2; i<=NF; i++) {printf "	"sprintf("%.2f", $i)}; print ""}}' | sed 's/	/|/g'`" \
  /home/efournie/genpipes/bfx/report/RnaSeq.raw_counts_metrics.md \
  > report/RnaSeq.raw_counts_metrics.md
raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done
)
raw_counts_metrics_4_JOB_ID=$(echo "rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cufflinks
#-------------------------------------------------------------------------------
STEP=cufflinks
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cufflinks_1_JOB_ID: cufflinks.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.CTRL
JOB_DEPENDENCIES=$bam_hard_clip_1_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.CTRL.8a2f3c37bcd103877d08fc325e3b2107.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.CTRL.8a2f3c37bcd103877d08fc325e3b2107.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/CTRL && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/CTRL \
  --num-threads 8 \
  alignment/CTRL/CTRL.sorted.mdup.hardClip.bam
cufflinks.CTRL.8a2f3c37bcd103877d08fc325e3b2107.mugqic.done
)
cufflinks_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_2_JOB_ID: cufflinks.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.L-FAT
JOB_DEPENDENCIES=$bam_hard_clip_2_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.L-FAT.4267a070a7adcdaa0c9cedcca181fbbe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.L-FAT.4267a070a7adcdaa0c9cedcca181fbbe.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/L-FAT && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/L-FAT \
  --num-threads 8 \
  alignment/L-FAT/L-FAT.sorted.mdup.hardClip.bam
cufflinks.L-FAT.4267a070a7adcdaa0c9cedcca181fbbe.mugqic.done
)
cufflinks_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_3_JOB_ID: cufflinks.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.H-FAT
JOB_DEPENDENCIES=$bam_hard_clip_3_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.H-FAT.cdfcafb664ba3d36317a6ee0cfbd75e5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.H-FAT.cdfcafb664ba3d36317a6ee0cfbd75e5.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/H-FAT && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/H-FAT \
  --num-threads 8 \
  alignment/H-FAT/H-FAT.sorted.mdup.hardClip.bam
cufflinks.H-FAT.cdfcafb664ba3d36317a6ee0cfbd75e5.mugqic.done
)
cufflinks_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_4_JOB_ID: cufflinks.CTRL.Ins
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.CTRL.Ins
JOB_DEPENDENCIES=$bam_hard_clip_4_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.CTRL.Ins.05e3c4fa027c2ee3ba685cace5a19996.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.CTRL.Ins.05e3c4fa027c2ee3ba685cace5a19996.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/CTRL.Ins && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/CTRL.Ins \
  --num-threads 8 \
  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.hardClip.bam
cufflinks.CTRL.Ins.05e3c4fa027c2ee3ba685cace5a19996.mugqic.done
)
cufflinks_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_5_JOB_ID: cufflinks.L-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.L-FAT.Ins
JOB_DEPENDENCIES=$bam_hard_clip_5_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.L-FAT.Ins.077915d798aa141d329f7355b878f327.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.L-FAT.Ins.077915d798aa141d329f7355b878f327.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/L-FAT.Ins && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/L-FAT.Ins \
  --num-threads 8 \
  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.hardClip.bam
cufflinks.L-FAT.Ins.077915d798aa141d329f7355b878f327.mugqic.done
)
cufflinks_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cufflinks_6_JOB_ID: cufflinks.H-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.H-FAT.Ins
JOB_DEPENDENCIES=$bam_hard_clip_6_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.H-FAT.Ins.39872995fec01d44e64f2f0df523c8d7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.H-FAT.Ins.39872995fec01d44e64f2f0df523c8d7.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/H-FAT.Ins && \
cufflinks -q  \
  --GTF-guide /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/H-FAT.Ins \
  --num-threads 8 \
  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.hardClip.bam
cufflinks.H-FAT.Ins.39872995fec01d44e64f2f0df523c8d7.mugqic.done
)
cufflinks_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cuffmerge
#-------------------------------------------------------------------------------
STEP=cuffmerge
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffmerge_1_JOB_ID: cuffmerge
#-------------------------------------------------------------------------------
JOB_NAME=cuffmerge
JOB_DEPENDENCIES=$cufflinks_1_JOB_ID:$cufflinks_2_JOB_ID:$cufflinks_3_JOB_ID:$cufflinks_4_JOB_ID:$cufflinks_5_JOB_ID:$cufflinks_6_JOB_ID
JOB_DONE=job_output/cuffmerge/cuffmerge.adb16bb3524819639e83932014483ff0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffmerge.adb16bb3524819639e83932014483ff0.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/AllSamples && \
`cat > cufflinks/cuffmerge.samples.txt << END
cufflinks/CTRL/transcripts.gtf
cufflinks/L-FAT/transcripts.gtf
cufflinks/H-FAT/transcripts.gtf
cufflinks/CTRL.Ins/transcripts.gtf
cufflinks/L-FAT.Ins/transcripts.gtf
cufflinks/H-FAT.Ins/transcripts.gtf
END

` && \
cuffmerge  \
  --ref-gtf /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.gtf \
  --ref-sequence /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  -o cufflinks/AllSamples \
  --num-threads 8 \
  cufflinks/cuffmerge.samples.txt
cuffmerge.adb16bb3524819639e83932014483ff0.mugqic.done
)
cuffmerge_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffmerge\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffmerge\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffmerge_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cuffquant
#-------------------------------------------------------------------------------
STEP=cuffquant
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffquant_1_JOB_ID: cuffquant.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.CTRL
JOB_DEPENDENCIES=$bam_hard_clip_1_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.CTRL.070a2afbd23b460094a8f9b60ee596c6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.CTRL.070a2afbd23b460094a8f9b60ee596c6.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/CTRL && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/CTRL \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/CTRL/CTRL.sorted.mdup.hardClip.bam
cuffquant.CTRL.070a2afbd23b460094a8f9b60ee596c6.mugqic.done
)
cuffquant_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_2_JOB_ID: cuffquant.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.L-FAT
JOB_DEPENDENCIES=$bam_hard_clip_2_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.L-FAT.15397f7b95ce9d0b4662095793fb223a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.L-FAT.15397f7b95ce9d0b4662095793fb223a.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/L-FAT && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/L-FAT \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/L-FAT/L-FAT.sorted.mdup.hardClip.bam
cuffquant.L-FAT.15397f7b95ce9d0b4662095793fb223a.mugqic.done
)
cuffquant_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_3_JOB_ID: cuffquant.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.H-FAT
JOB_DEPENDENCIES=$bam_hard_clip_3_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.H-FAT.4dfb7d92e47b4e44ecadef64dc91ca53.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.H-FAT.4dfb7d92e47b4e44ecadef64dc91ca53.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/H-FAT && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/H-FAT \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/H-FAT/H-FAT.sorted.mdup.hardClip.bam
cuffquant.H-FAT.4dfb7d92e47b4e44ecadef64dc91ca53.mugqic.done
)
cuffquant_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_4_JOB_ID: cuffquant.CTRL.Ins
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.CTRL.Ins
JOB_DEPENDENCIES=$bam_hard_clip_4_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.CTRL.Ins.7a0fb6edd3c2697f3653334a2c20647a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.CTRL.Ins.7a0fb6edd3c2697f3653334a2c20647a.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/CTRL.Ins && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/CTRL.Ins \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/CTRL.Ins/CTRL.Ins.sorted.mdup.hardClip.bam
cuffquant.CTRL.Ins.7a0fb6edd3c2697f3653334a2c20647a.mugqic.done
)
cuffquant_4_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_5_JOB_ID: cuffquant.L-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.L-FAT.Ins
JOB_DEPENDENCIES=$bam_hard_clip_5_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.L-FAT.Ins.43c533338c4e01cb679b13fe6812a914.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.L-FAT.Ins.43c533338c4e01cb679b13fe6812a914.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/L-FAT.Ins && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/L-FAT.Ins \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/L-FAT.Ins/L-FAT.Ins.sorted.mdup.hardClip.bam
cuffquant.L-FAT.Ins.43c533338c4e01cb679b13fe6812a914.mugqic.done
)
cuffquant_5_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffquant_6_JOB_ID: cuffquant.H-FAT.Ins
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.H-FAT.Ins
JOB_DEPENDENCIES=$bam_hard_clip_6_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.H-FAT.Ins.5b7a36032b8ae3d2dbcde5589764415f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.H-FAT.Ins.5b7a36032b8ae3d2dbcde5589764415f.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/H-FAT.Ins && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/H-FAT.Ins \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/H-FAT.Ins/H-FAT.Ins.sorted.mdup.hardClip.bam
cuffquant.H-FAT.Ins.5b7a36032b8ae3d2dbcde5589764415f.mugqic.done
)
cuffquant_6_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cuffdiff
#-------------------------------------------------------------------------------
STEP=cuffdiff
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffdiff_1_JOB_ID: cuffdiff.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.CTRL
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_4_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.CTRL.468100567f9a8712253feb8303c465e5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.CTRL.468100567f9a8712253feb8303c465e5.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/CTRL && \
cuffdiff -u \
  --frag-bias-correct /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/CTRL \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/CTRL/abundances.cxb \
  cufflinks/CTRL.Ins/abundances.cxb
cuffdiff.CTRL.468100567f9a8712253feb8303c465e5.mugqic.done
)
cuffdiff_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffdiff_2_JOB_ID: cuffdiff.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.L-FAT
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_5_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.L-FAT.e222b34eecee714dc64f1c1722b162c9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.L-FAT.e222b34eecee714dc64f1c1722b162c9.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/L-FAT && \
cuffdiff -u \
  --frag-bias-correct /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/L-FAT \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/L-FAT/abundances.cxb \
  cufflinks/L-FAT.Ins/abundances.cxb
cuffdiff.L-FAT.e222b34eecee714dc64f1c1722b162c9.mugqic.done
)
cuffdiff_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: cuffdiff_3_JOB_ID: cuffdiff.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.H-FAT
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_6_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.H-FAT.873da892752396ba8c48429b584dec04.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.H-FAT.873da892752396ba8c48429b584dec04.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/H-FAT && \
cuffdiff -u \
  --frag-bias-correct /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/H-FAT \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/H-FAT/abundances.cxb \
  cufflinks/H-FAT.Ins/abundances.cxb
cuffdiff.H-FAT.873da892752396ba8c48429b584dec04.mugqic.done
)
cuffdiff_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: cuffnorm
#-------------------------------------------------------------------------------
STEP=cuffnorm
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffnorm_1_JOB_ID: cuffnorm
#-------------------------------------------------------------------------------
JOB_NAME=cuffnorm
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_4_JOB_ID:$cuffquant_5_JOB_ID:$cuffquant_6_JOB_ID
JOB_DONE=job_output/cuffnorm/cuffnorm.ebbf265d6ce124863e7af166825df120.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffnorm.ebbf265d6ce124863e7af166825df120.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffnorm && \
cuffnorm -q  \
  --library-type fr-firststrand \
  --output-dir cuffnorm \
  --num-threads 8 \
  --labels CTRL,L-FAT,H-FAT,CTRL.Ins,L-FAT.Ins,H-FAT.Ins \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/CTRL/abundances.cxb \
  cufflinks/L-FAT/abundances.cxb \
  cufflinks/H-FAT/abundances.cxb \
  cufflinks/CTRL.Ins/abundances.cxb \
  cufflinks/L-FAT.Ins/abundances.cxb \
  cufflinks/H-FAT.Ins/abundances.cxb
cuffnorm.ebbf265d6ce124863e7af166825df120.mugqic.done
)
cuffnorm_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffnorm\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"cuffnorm\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffnorm_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: fpkm_correlation_matrix
#-------------------------------------------------------------------------------
STEP=fpkm_correlation_matrix
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: fpkm_correlation_matrix_1_JOB_ID: fpkm_correlation_matrix_transcript
#-------------------------------------------------------------------------------
JOB_NAME=fpkm_correlation_matrix_transcript
JOB_DEPENDENCIES=$cuffnorm_1_JOB_ID
JOB_DONE=job_output/fpkm_correlation_matrix/fpkm_correlation_matrix_transcript.dd36f12d31d4efce73062b0bf461e872.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'fpkm_correlation_matrix_transcript.dd36f12d31d4efce73062b0bf461e872.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics && \
R --no-save --no-restore <<-EOF
dataFile=read.table("cuffnorm/isoforms.fpkm_table",header=T,check.names=F)
fpkm=cbind(dataFile[,2:ncol(dataFile)])
corTable=cor(log2(fpkm+0.1))
corTableOut=rbind(c('Vs.',colnames(corTable)),cbind(rownames(corTable),round(corTable,3)))
write.table(corTableOut,file="metrics/transcripts_fpkm_correlation_matrix.tsv",col.names=F,row.names=F,sep="	",quote=F)
print("done.")

EOF
fpkm_correlation_matrix_transcript.dd36f12d31d4efce73062b0bf461e872.mugqic.done
)
fpkm_correlation_matrix_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"fpkm_correlation_matrix\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"fpkm_correlation_matrix\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$fpkm_correlation_matrix_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: fpkm_correlation_matrix_2_JOB_ID: fpkm_correlation_matrix_gene
#-------------------------------------------------------------------------------
JOB_NAME=fpkm_correlation_matrix_gene
JOB_DEPENDENCIES=$cuffnorm_1_JOB_ID
JOB_DONE=job_output/fpkm_correlation_matrix/fpkm_correlation_matrix_gene.60f3f6a5d19daa981493a76f44f36b5e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'fpkm_correlation_matrix_gene.60f3f6a5d19daa981493a76f44f36b5e.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 && \
R --no-save --no-restore <<-EOF
dataFile=read.table("cuffnorm/genes.fpkm_table",header=T,check.names=F)
fpkm=cbind(dataFile[,2:ncol(dataFile)])
corTable=cor(log2(fpkm+0.1))
corTableOut=rbind(c('Vs.',colnames(corTable)),cbind(rownames(corTable),round(corTable,3)))
write.table(corTableOut,file="metrics/gene_fpkm_correlation_matrix.tsv",col.names=F,row.names=F,sep="	",quote=F)
print("done.")

EOF
fpkm_correlation_matrix_gene.60f3f6a5d19daa981493a76f44f36b5e.mugqic.done
)
fpkm_correlation_matrix_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"fpkm_correlation_matrix\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"fpkm_correlation_matrix\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$fpkm_correlation_matrix_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: gq_seq_utils_exploratory_analysis_rnaseq
#-------------------------------------------------------------------------------
STEP=gq_seq_utils_exploratory_analysis_rnaseq
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID: gq_seq_utils_exploratory_analysis_rnaseq
#-------------------------------------------------------------------------------
JOB_NAME=gq_seq_utils_exploratory_analysis_rnaseq
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID:$cuffnorm_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/gq_seq_utils_exploratory_analysis_rnaseq.837dbdd05e1a8f631909161ce320e63b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'gq_seq_utils_exploratory_analysis_rnaseq.837dbdd05e1a8f631909161ce320e63b.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 mugqic/mugqic_R_packages/1.0.5 && \
mkdir -p exploratory && \
R --no-save --no-restore <<-EOF
suppressPackageStartupMessages(library(gqSeqUtils))

exploratoryAnalysisRNAseq(htseq.counts.path="DGE/rawCountMatrix.csv", cuffnorm.fpkms.dir="cuffnorm", genes.path="/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.genes.tsv", output.dir="exploratory")
desc = readRDS(file.path("exploratory","index.RData"))
write.table(desc,file=file.path("exploratory","index.tsv"),sep='	',quote=F,col.names=T,row.names=F)
print("done.")

EOF
gq_seq_utils_exploratory_analysis_rnaseq.837dbdd05e1a8f631909161ce320e63b.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"gq_seq_utils_exploratory_analysis_rnaseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"gq_seq_utils_exploratory_analysis_rnaseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=00:30:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID: gq_seq_utils_exploratory_analysis_rnaseq_report
#-------------------------------------------------------------------------------
JOB_NAME=gq_seq_utils_exploratory_analysis_rnaseq_report
JOB_DEPENDENCIES=$gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/gq_seq_utils_exploratory_analysis_rnaseq_report.29278144299c5c599281811b9a248725.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'gq_seq_utils_exploratory_analysis_rnaseq_report.29278144299c5c599281811b9a248725.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 mugqic/pandoc/1.15.2 && \
R --no-save --no-restore <<-'EOF'
report_dir="report";
input_rmarkdown_file = '/home/efournie/genpipes/bfx/report/RnaSeq.gq_seq_utils_exploratory_analysis_rnaseq.Rmd'
render_output_dir    = 'report'
rmarkdown_file       = basename(input_rmarkdown_file) # honoring a different WD that location of Rmd file in knitr is problematic
file.copy(from = input_rmarkdown_file, to = rmarkdown_file, overwrite = T)
rmarkdown::render(input = rmarkdown_file, output_format = c("html_document","md_document"), output_dir = render_output_dir  )
file.remove(rmarkdown_file)
EOF
gq_seq_utils_exploratory_analysis_rnaseq_report.29278144299c5c599281811b9a248725.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID=$(echo "rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID: cuffnorm_report
#-------------------------------------------------------------------------------
JOB_NAME=cuffnorm_report
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/cuffnorm_report.59777e2c95a1c5c6e936bd8d1f54682f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffnorm_report.59777e2c95a1c5c6e936bd8d1f54682f.mugqic.done'
mkdir -p report && \
zip -r report/cuffAnalysis.zip cufflinks/ cuffdiff/ cuffnorm/ && \
cp \
  /home/efournie/genpipes/bfx/report/RnaSeq.cuffnorm.md \
  report/RnaSeq.cuffnorm.md
cuffnorm_report.59777e2c95a1c5c6e936bd8d1f54682f.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID=$(echo "rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: differential_expression
#-------------------------------------------------------------------------------
STEP=differential_expression
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: differential_expression_1_JOB_ID: differential_expression
#-------------------------------------------------------------------------------
JOB_NAME=differential_expression
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID
JOB_DONE=job_output/differential_expression/differential_expression.42a7012dd3cf28bbb15a517aa3543cd2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'differential_expression.42a7012dd3cf28bbb15a517aa3543cd2.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p DGE && \
Rscript $R_TOOLS/edger.R \
  -d ../../../../project/6009125/efournie/HumanGranulosa/raw/design_fat.txt \
  -c DGE/rawCountMatrix.csv \
  -o DGE && \
Rscript $R_TOOLS/deseq.R \
  -d ../../../../project/6009125/efournie/HumanGranulosa/raw/design_fat.txt \
  -c DGE/rawCountMatrix.csv \
  -o DGE \
  
differential_expression.42a7012dd3cf28bbb15a517aa3543cd2.mugqic.done
)
differential_expression_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"differential_expression\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"differential_expression\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.json,/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json,/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json,/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=10:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$differential_expression_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: differential_expression_goseq
#-------------------------------------------------------------------------------
STEP=differential_expression_goseq
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: differential_expression_goseq_1_JOB_ID: differential_expression_goseq.dge.CTRL
#-------------------------------------------------------------------------------
JOB_NAME=differential_expression_goseq.dge.CTRL
JOB_DEPENDENCIES=$differential_expression_1_JOB_ID
JOB_DONE=job_output/differential_expression_goseq/differential_expression_goseq.dge.CTRL.a8b294d1da83b25ed6bb5da659be1214.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'differential_expression_goseq.dge.CTRL.a8b294d1da83b25ed6bb5da659be1214.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 mugqic/R_Bioconductor/3.4.2_3.6 && \
Rscript $R_TOOLS/goseq.R -p 0.1 -f 0.1 \
  -a /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.genes.length.tsv \
  -G /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.GO.tsv \
  -d DGE/CTRL/dge_results.csv \
  -c 1,6 \
  -o DGE/CTRL/gene_ontology_results.csv
differential_expression_goseq.dge.CTRL.a8b294d1da83b25ed6bb5da659be1214.mugqic.done
)
differential_expression_goseq_1_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"differential_expression_goseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"differential_expression_goseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/CTRL.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=10:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$differential_expression_goseq_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: differential_expression_goseq_2_JOB_ID: differential_expression_goseq.dge.L-FAT
#-------------------------------------------------------------------------------
JOB_NAME=differential_expression_goseq.dge.L-FAT
JOB_DEPENDENCIES=$differential_expression_1_JOB_ID
JOB_DONE=job_output/differential_expression_goseq/differential_expression_goseq.dge.L-FAT.40612ef26d417e4f1a7c3e7d205daee3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'differential_expression_goseq.dge.L-FAT.40612ef26d417e4f1a7c3e7d205daee3.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 mugqic/R_Bioconductor/3.4.2_3.6 && \
Rscript $R_TOOLS/goseq.R -p 0.1 -f 0.1 \
  -a /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.genes.length.tsv \
  -G /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.GO.tsv \
  -d DGE/L-FAT/dge_results.csv \
  -c 1,6 \
  -o DGE/L-FAT/gene_ontology_results.csv
differential_expression_goseq.dge.L-FAT.40612ef26d417e4f1a7c3e7d205daee3.mugqic.done
)
differential_expression_goseq_2_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"differential_expression_goseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"differential_expression_goseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/L-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=10:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$differential_expression_goseq_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: differential_expression_goseq_3_JOB_ID: differential_expression_goseq.dge.H-FAT
#-------------------------------------------------------------------------------
JOB_NAME=differential_expression_goseq.dge.H-FAT
JOB_DEPENDENCIES=$differential_expression_1_JOB_ID
JOB_DONE=job_output/differential_expression_goseq/differential_expression_goseq.dge.H-FAT.02463e2eb0e67b0b7e6dd6514a077b50.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'differential_expression_goseq.dge.H-FAT.02463e2eb0e67b0b7e6dd6514a077b50.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 mugqic/R_Bioconductor/3.4.2_3.6 && \
Rscript $R_TOOLS/goseq.R -p 0.1 -f 0.1 \
  -a /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.genes.length.tsv \
  -G /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl87.GO.tsv \
  -d DGE/H-FAT/dge_results.csv \
  -c 1,6 \
  -o DGE/H-FAT/gene_ontology_results.csv
differential_expression_goseq.dge.H-FAT.02463e2eb0e67b0b7e6dd6514a077b50.mugqic.done
)
differential_expression_goseq_3_JOB_ID=$(echo "rm -f $JOB_DONE && module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"differential_expression_goseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13
$MUGQIC_PIPELINES_HOME/utils/job2json.py \
  -s \"differential_expression_goseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/output/chip-pipeline/json/H-FAT.Ins.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 
if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=10:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$differential_expression_goseq_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: differential_expression_goseq_4_JOB_ID: differential_expression_goseq_report
#-------------------------------------------------------------------------------
JOB_NAME=differential_expression_goseq_report
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID:$cuffdiff_1_JOB_ID:$cuffdiff_2_JOB_ID:$cuffdiff_3_JOB_ID:$differential_expression_1_JOB_ID:$differential_expression_goseq_1_JOB_ID:$differential_expression_goseq_2_JOB_ID:$differential_expression_goseq_3_JOB_ID
JOB_DONE=job_output/differential_expression_goseq/differential_expression_goseq_report.b4cab2a623acfb6786c95d26162d9487.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'differential_expression_goseq_report.b4cab2a623acfb6786c95d26162d9487.mugqic.done'
module load mugqic/python/2.7.13 mugqic/pandoc/1.15.2 && \
set -eu -o pipefail && \
mkdir -p report && \
cp /project/6009125/efournie/HumanGranulosa/raw/design_fat.txt report/design.tsv && \
cp DGE/rawCountMatrix.csv report/ && \
pandoc \
  /home/efournie/genpipes/bfx/report/RnaSeq.differential_expression.md \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.differential_expression.md \
  --variable design_table="`head -7 report/design.tsv | cut -f-8 | awk -F"	" '{OFS="	"; if (NR==1) {print; gsub(/[^	]/, "-")} print}' | sed 's/	/|/g'`" \
  --variable raw_count_matrix_table="`head -7 report/rawCountMatrix.csv | cut -f-8 | awk -F"	" '{OFS="	"; if (NR==1) {print; gsub(/[^	]/, "-")} print}' | sed 's/	/|/g'`" \
  --variable adj_pvalue_threshold=0.1 \
  --to markdown \
  > report/RnaSeq.differential_expression.md && \
for contrast in CTRL L-FAT H-FAT
do
  mkdir -p report/DiffExp/$contrast/
  echo -e "\n#### $contrast Results\n" >> report/RnaSeq.differential_expression.md
  cp DGE/$contrast/dge_results.csv report/DiffExp/$contrast/${contrast}_Genes_DE_results.tsv
  echo -e "\nTable: Differential Gene Expression Results (**partial table**; [download full table](DiffExp/$contrast/${contrast}_Genes_DE_results.tsv))\n" >> report/RnaSeq.differential_expression.md
  head -7 report/DiffExp/$contrast/${contrast}_Genes_DE_results.tsv | cut -f-8 | sed '2i ---	---	---	---	---	---	---	---' | sed 's/	/|/g' >> report/RnaSeq.differential_expression.md
  sed '1s/^tracking_id/test_id/' cuffdiff/$contrast/isoforms.fpkm_tracking | awk -F"	" 'FNR==NR{line[$1]=$0; next}{OFS="	"; print line[$1], $0}' - cuffdiff/$contrast/isoform_exp.diff | python -c 'import csv,sys; rows_in = csv.DictReader(sys.stdin, delimiter="	"); rows_out = csv.DictWriter(sys.stdout, fieldnames=["test_id", "gene_id", "tss_id","nearest_ref_id","class_code","gene","locus","length","log2(fold_change)","test_stat","p_value","q_value"], delimiter="	", extrasaction="ignore"); rows_out.writeheader(); rows_out.writerows(rows_in)' > report/DiffExp/$contrast/${contrast}_Transcripts_DE_results.tsv
  echo -e "\n---\n\nTable: Differential Transcript Expression Results (**partial table**; [download full table](DiffExp/$contrast/${contrast}_Transcripts_DE_results.tsv))\n" >> report/RnaSeq.differential_expression.md
  head -7 report/DiffExp/$contrast/${contrast}_Transcripts_DE_results.tsv | cut -f-8 | sed '2i ---	---	---	---	---	---	---	---' | sed 's/	/|/g' >> report/RnaSeq.differential_expression.md
  if [ `wc -l DGE/$contrast/gene_ontology_results.csv | cut -f1 -d\ ` -gt 1 ]
  then
    cp DGE/$contrast/gene_ontology_results.csv report/DiffExp/$contrast/${contrast}_Genes_GO_results.tsv
    echo -e "\n---\n\nTable: GO Results of the Differentially Expressed Genes (**partial table**; [download full table](DiffExp/${contrast}/${contrast}_Genes_GO_results.tsv))\n" >> report/RnaSeq.differential_expression.md
    head -7 report/DiffExp/${contrast}/${contrast}_Genes_GO_results.tsv | cut -f-8 | sed '2i ---	---	---	---	---	---	---	---' | sed 's/	/|/g' >> report/RnaSeq.differential_expression.md
  else
    echo -e "\nNo FDR adjusted GO enrichment was significant (p-value too high) based on the differentially expressed gene results for this design.\n" >> report/RnaSeq.differential_expression.md
  fi
done
differential_expression_goseq_report.b4cab2a623acfb6786c95d26162d9487.mugqic.done
)
differential_expression_goseq_4_JOB_ID=$(echo "rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then
  touch $JOB_DONE ;
fi
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$differential_expression_goseq_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar5.cedar.computecanada.ca&ip=206.12.124.6&pipeline=RnaSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,star,picard_merge_sam_files,picard_sort_sam,picard_mark_duplicates,picard_rna_metrics,estimate_ribosomal_rna,bam_hard_clip,rnaseqc,wiggle,raw_counts,raw_counts_metrics,cufflinks,cuffmerge,cuffquant,cuffdiff,cuffnorm,fpkm_correlation_matrix,gq_seq_utils_exploratory_analysis_rnaseq,differential_expression,differential_expression_goseq&samples=6&AnonymizedList=c05cfb27f86c4aea922fa54fc789ea53,078d29b4a17f3300e3a223094ba56b77,1fcec59b40bfc3f7cddf9dda0441d6c3,f4ddf91dc50db489a2768e0a857f0907,98a6a3cea4bbc1ce8df1ee03366492b6,f6181beb5a88e86eabf44ca43624491c" --quiet --output-document=/dev/null

