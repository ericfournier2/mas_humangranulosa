export RAP_ID=def-masirard

OUTDIR=output/fastqc
mkdir -p $OUTDIR
mkdir -p output/jobs
for libfile in output/trim/*/*pair*.fastq.gz
do
  bn=`basename $libfile .fastq.gz`
  job_name=fastqc.$bn
  job_script=output/jobs/$job_name.sh
  
  cat << EOF > $job_script
#!/bin/bash
module load fastqc
mkdir -p $OUTDIR/$bn
fastqc -o $OUTDIR/$bn $libfile
EOF

  sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D `pwd` -o $job_script.stdout -e $job_script.stderr -J $job_name --time=24:00:0 --mem=24G -N 1 -n 6 $job_script

done
